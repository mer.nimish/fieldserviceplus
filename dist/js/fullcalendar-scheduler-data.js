$(function() { // document ready


    /* initialize the external events
    -----------------------------------------------------------------*/

    $('.calendar-event').each(function() {

      // store data so the calendar knows to render an event upon drop
      $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        stick: true // maintain when user navigates (see docs on the renderEvent method)
      });

      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });

    });
  
    /* initialize the calendar
    -----------------------------------------------------------------*/

    $('#calendar').fullCalendar({
      schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
      now: new Date(),
      editable: true, // enable draggable events
      droppable: true, // this allows things to be dropped onto the calendar
      aspectRatio: 1.8,
      scrollTime: '00:00', // undo default 6am scrollTime
      header: false,
      titleFormat: 'MMMM Do, YYYY',
       header: {
        left: 'prev,next today',
        center: 'title',
        /*right: 'timelineDay,timelineWeek,'*/
      },
      defaultView: 'timelineDay',
      views: {
        timelineWeek: {
          type: 'timeline',
          duration: { days: 7 },
          titleFormat: 'MMMM Do, YYYY',
      /*  slotDuration: '08:00',
          slotLabelInterval:'06:00',
          snapDuration : '07:00:00',
          minTime: "07:00:00",
          maxTime: "24:00:00",*/
        }
      },
      viewRender: function(view) {
        setTimeout(function(){ 
          var title = view.title.replace("â€“", '-');
          $(".showdatefulldate").html(title); 
          $(".fc-center h2").html(title);

        }, 50);
        
      },
      resourceLabelText: 'Assigned To',
      resources: [
        { id: 'a', title: 'Michael Parry', eventColor: '#5fc55f' },
        { id: 'b', title: 'Nimish Mer', eventColor: '#5fc55f' },
        { id: 'c', title: 'Technician #3', eventColor: '#5fc55f' },
        { id: 'e', title: 'Technician #4', eventColor: '#5fc55f' },
        { id: 'j', title: 'Unassigned Work Orders' },
      ],
      events: [
        { id: '1', resourceId: 'b', start: '2018-05-29T00:00:00', end: '2018-05-29T00:30:00', title: ' WO-001555 PM Maintenance Danville, CA' },
        { id: '2', resourceId: 'c', start: '2018-05-29T05:00:00', end: '2018-05-29T07:00:00', title: 'WO-001998 HVAC Installation Danville, CA' },
        { id: '3', resourceId: 'd', start: '2018-05-29', end: '2018-05-29', title: 'WO-001255 HVAC Installation Danville, CA' },
        { id: '4', resourceId: 'e', start: '2018-05-30T03:00:00', end: '2018-05-30T08:00:00', title: 'WO-001889 HVAC Installation Danville, CA' },
        { id: '5', resourceId: 'f', start: '2018-05-30T00:30:00', end: '2018-05-30T02:30:00', title: 'WO-001555 HVAC Installation Danville, CA' }
      ],
      drop: function(date, jsEvent, ui, resourceId) {
        console.log('drop', date.format(), resourceId);

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
      },
      eventReceive: function(event) { // called when a proper external event is dropped
        console.log('eventReceive', event);
      },
      eventDrop: function(event) { // called when an event (already on the calendar) is moved
        console.log('eventDrop', event);
      }
    });

});