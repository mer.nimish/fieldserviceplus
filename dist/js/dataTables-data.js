/*DataTable Init*/

"use strict"; 

$(document).ready(function() {
	"use strict";

	$('#datable_1').DataTable( {
	  "searching": false,
	  "columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ]
	});
    $('#datable_2').DataTable({ "lengthChange": false});
} );