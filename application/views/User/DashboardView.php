
<?php $this->load->view('Template/HeaderView')?>
 
<link href="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>

            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">My Tasks and Events</h6>
									</div>
									<div class="pull-right">
										<select class="selectpicker" data-style="form-control btn-default btn-outline">
											<option value="">Today</option>
											<option value="">Today + Overdue</option>
											<option value="" style="border-bottom: 1px solid gainsboro;padding-bottom: 6px;">Overdue</option>
											<option value="">Jump to Calendar</option>
											<option value="">Jump to Tasks</option>
										</select>
									</div>	
									<div class="clearfix"></div>
								</div>
								<div  class="panel-wrapper collapse in">
									<div  class="panel-body" style="height:385px;">
										<ul class="list-icons">
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 1/3/2018 PM Visit to Frank Wright</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 1/2/2018 PM Visit to Joe Olson</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 1/1/2018	New Installation of Heating Unit - Sue White </li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 12/31/2017 Call John Smith</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 12/23/2017 Repair HVAC</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 12/10/2017 Repair HVAC</li>
										</ul>
									</div>
								</div>
						</div>
					</div>
					
					<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="calendar-wrap">
									  <div id="calendar_small" class="small-calendar"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->


				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Dashboard</h6>
								</div>
								<div class="pull-right">
									<a href="#" class="pull-left inline-block full-screen mr-15">
										<i class="zmdi zmdi-fullscreen"></i>
									</a>
									<!-- <div class="pull-left inline-block dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
										<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
											<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Devices</a></li>
											<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>General</a></li>
											<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>Referral</a></li>
										</ul>
									</div> -->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="e_chart_3" class="" style="height:330px;"></div>
								</div>
							</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="bar_chart_horizontal" class="" style="height:346px;"></div>
								</div>
							</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<canvas id="bar_chart_vertical" height="200"></canvas>
								</div>	
							</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
				
			
			</div>
			

 <?php $this->load->view('Template/FooterView')?>
 
<!-- Calender JavaScripts -->
<script src="<?=SITE_URL?>vendors/bower_components/moment/min/moment.min.js"></script>
<script src="<?=SITE_URL?>vendors/jquery-ui.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?=SITE_URL?>dist/js/fullcalendar-data.js"></script>

<!-- EChartJS JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
<script src="<?=SITE_URL?>vendors/echarts-liquidfill.min.js"></script>
<script src="<?=SITE_URL?>vendors/ecStat.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="<?=SITE_URL?>vendors/chart.js/Chart.min.js"></script>

<!-- Init JavaScript -->
<script src="<?=SITE_URL?>dist/js/home.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>