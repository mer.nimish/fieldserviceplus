<?php   ?>
<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><?=MAINTITLE;?></title>
	<meta name="description" content="<?=MAINTITLE;?> is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Field Service Pulse Admin, Field Service Pulseadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- select2 CSS -->
	<link href="<?=SITE_URL?>vendors/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>

	<!-- bootstrap-touchspin CSS -->
	<link href="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>

	<!-- Toast JavaScript -->
	<link href="<?=SITE_URL;?>vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">  

	<!-- bootstrap-select CSS -->
	<link href="<?=SITE_URL;?>vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>

	<!-- Data table CSS -->
	<link href="<?=SITE_URL;?>vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=SITE_URL;?>vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>
 
	<!-- Calendar CSS -->
	<!-- <link href="vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/> -->

	<!-- bootstrap datepicker CSS -->
	<link href="<?=SITE_URL;?>dist/css/datepicker.css" rel="stylesheet" type="text/css" />

	<!-- Custom CSS -->
	<link href="<?=SITE_URL;?>dist/css/style.css" rel="stylesheet" type="text/css">
	
</head>

<body>
	<!-- Preloader -->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!-- /Preloader -->
    <div class="wrapper theme-5-active pimary-color-blue">
		<!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="<?=SITE_URL?>User/Home">
							<img class="brand-img" src="<?=SITE_LOGO_URL?>" alt="brand"/>
						 	<span class="brand-text">Field Service</span> 
							
						</a>
					</div>
				</div>	
				 <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left " href="javascript:void(0);"><i class="zmdi zmdi-menu toggle_nav_btn_for_mobile" style="display: none;"></i></a>  
				<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
				<form id="search_form" role="search" class="top-nav-search collapse pull-left">
					<div class="input-group">
						<input type="text" name="example-input1-group2" class="form-control" placeholder="Search">
						<span class="input-group-btn">
						<button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
						</span>
					</div>
				</form>
			</div>
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">
				   <li>
						<a id="open_right_sidebar" href="#"><i class="fa fa-question-circle-o top-nav-icon"></i></a>
					</li>
					
					<li class="">
						<a href="#" class="dropdown-toggle pr-2" data-toggle="dropdown"><i class="fa fa-plus-square-o top-nav-icon"></i></a>
						<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
						    <li>
								<a class="pr-2">CREATE NEW</a>
							</li>
							<li>
								<a href="<?=SITE_URL?>Account/Actions/CreateAccount"><i class="fa fa-user"></i><span>Account</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-user"></i><span>Contact</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-calendar"></i><span>Event</span></a>
							</li>
							<li>
								<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder"><i class="fa fa-first-order"></i><span>Work Order</span></a>
							</li>
							<li>
								<a href="#"><i class="zmdi zmdi-flag"></i><span>Estimate</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-file-pdf-o"></i><span>Invoice</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-tasks"></i><span>Task</span></a>
							</li>
						</ul>
					</li>
				   <li>
						<a id="open_right_sidebar" href="#"><i class="zmdi zmdi-settings top-nav-icon"></i></a>
					</li>
					<li class="dropdown auth-drp">
						<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="<?=SITE_URL?>img/user1.png" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
						<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
							<li>
								<a href="#"><i class="zmdi zmdi-account"></i><span><?=$this->session->userdata('FirstName').' '.$this->session->userdata('LastName');?></span></a>
							</li>
							<li>
								<a href="#"><i class="zmdi zmdi-settings"></i><span>Settings</span></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?=SITE_URL?>/User/Auth/Logout"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
							</li>
						</ul>
					</li>
				</ul>
			</div>	
		</nav>
		<!-- /Top Menu Items -->



		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				 <li class="navigation-header">
					<span> Main 
					 <a style="width: 0;" id="toggle_nav_btn" class="toggle-right-nav-btn pull-right" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>  
					 </span> 
					<i id="toggle_nav_btn" class="zmdi zmdi-menu"></i>  
				</li>
				<li>
					<a href="<?=SITE_URL?>User/Home"><div class="pull-left"><i class="fa fa-home mr-20"></i><span class="right-nav-text">Home</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="<?=SITE_URL?>Account/Actions"><div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Accounts</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="#"><div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Contacts</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="#"><div class="pull-left"><i class="fa fa-calendar mr-20"></i><span class="right-nav-text">Calendar</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="<?=SITE_URL?>WorkOrder/Actions"><div class="pull-left"><i class="fa fa-first-order mr-20"></i><span class="right-nav-text">Work Order</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="#"><div class="pull-left"><i class="zmdi zmdi-flag mr-20"></i><span class="right-nav-text">Estimates</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="#"><div class="pull-left"><i class="fa fa-file-pdf-o mr-20"></i><span class="right-nav-text">Invoices</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="#"><div class="pull-left"><i class="fa fa-file mr-20"></i><span class="right-nav-text">Files</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="#"><div class="pull-left"><i class="fa fa-tasks mr-20"></i><span class="right-nav-text">Tasks</span></div><div class="clearfix"></div></a>
				</li>

			</ul>
		</div>
		<!-- /Left Sidebar Menu -->


        <!-- Main Content -->
		<div class="page-wrapper">
		
		