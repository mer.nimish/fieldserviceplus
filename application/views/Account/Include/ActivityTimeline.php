<div class="panel panel-default card-view">
	<div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Activity Timeline</h6>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-wrapper collapse in">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12 text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-outline" data-toggle="modal" data-target="#CreateNewTask">New Task</button>
							<button type="button" class="btn btn-default btn-outline" data-toggle="modal" data-target="#CreateNewEvent">New Event</button>
						</div>
				</div>

				<div class="col-md-12 mt-10">
					<div class="panel-heading">
						<div class="pull-left">
							<div class="txt-dark"><strong>Open Activities</strong></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					<table class="table table-striped table-bordered mb-0">
							<thead>
								<tr>
									<th>Subject</th>
									<th width="20%">Type</th>
									<th width="35%">Date</th>
								</tr>
							</thead>
							<tbody>
							<?php $x = 1; 
							foreach ($OpenActivities as $key => $OpenActivity) { ?>
									<tr>
										<td><a href="#"><?=$OpenActivity['Subject']?></a></td>
										<td><?=$OpenActivity['Type']?></td>
										<td><?=$OpenActivity['Date']?></td>
									</tr>
							<?php $x++; } 

								for($y = $x;$y <= 3;$y++){?>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr> 
							<?php } ?>
							</tbody>
						</table>
					<div class="clearfix"></div>
					<?php if($x > 10) { ?>
					<div class="pull-right pt-10">
						<a href="<?=SITE_URL?>Account/Actions/OpenActivities/<?=$AccountData->AccountID;?>" class="btn btn-default btn-outline">View All</a>
					</div>
					<?php } ?>
				</div>
			
				<div class="col-md-12 mt-10">
					<div class="panel-heading">
						<div class="pull-left">
							<div class="txt-dark"><strong>Completed Activities</strong></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<table class="table table-striped table-bordered mb-0">
							<thead>
								<tr>
									<th>Subject</th>
									<th width="20%">Type</th>
									<th width="35%">Date</th>
								</tr>
							</thead>
							<tbody> 
								<?php $x = 1; 
									foreach ($CompletedActivities as $key => $CompletedActivity) { ?>
									<tr>
										<td><a href="#"><?=$CompletedActivity['Subject']?></a></td>
										<td><?=$CompletedActivity['Type']?></td>
										<td><?=$CompletedActivity['Date']?></td>
									</tr>
							<?php $x++; } 

							for($y = $x;$y <= 3;$y++){?>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr> 
							<?php } ?>
							</tbody>
						</table>
						<div class="clearfix"></div>
						<?php if($x > 10) { ?>
						<div class="pull-right pt-10">
							<a href="<?=SITE_URL?>Account/Actions/CompletedActivities/<?=$AccountData->AccountID;?>" class="btn btn-default btn-outline">View All</a>
						</div>
						<?php } ?>
					</div>
			</div>
		</div>
	</div>
</div>