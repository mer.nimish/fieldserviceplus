
<?php $this->load->view('Template/HeaderView');?>
 
<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

        <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								
								<div class="form-group">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<label class="control-label mt-10"><strong>View:</strong></label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pr-0">
									<form method="post" action="<?=SITE_URL?>WorkOrder/View/" id="SelectedAcForm">
										<select class="selectpicker" name="SelectedWorkOrder" data-style="form-control btn-default btn-outline" onchange="ViewWorkOrders(this.value);">
											<option disabled="" selected="">Select Views</option>
											<optgroup label="Default Views">
											<option value="MyOpenWorkOrdersToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='MyOpenWorkOrdersToday')?"selected":""?>>My Open Work Orders Today</option>
											<option value="MyOpenWorkOrdersThisWeek" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='MyOpenWorkOrdersThisWeek')?"selected":""?>>My Open Work Orders This Week</option>
											<option value="AllOpenWorkOrdersToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllOpenWorkOrdersToday')?"selected":""?>>All Open Work Orders Today</option>
											<option value="AllWorkOrdersToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllWorkOrdersToday')?"selected":""?>>All Work Orders Today</option>
											<option value="AllOpenWorkOrdersThisWeek" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllOpenWorkOrdersThisWeek')?"selected":""?>>All Open Work Orders This Week</option>
											<option value="WorkOrdersCreatedToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='WorkOrdersCreatedToday')?"selected":""?>>Work Orders Created Today</option>
											<option value="WorkOrdersCreatedThisWeek" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='WorkOrdersCreatedThisWeek')?"selected":""?>>Work Orders Created This Week</option>
											<option value="UnscheduledWorkOrders" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='UnscheduledWorkOrders')?"selected":""?>>Unscheduled Work Orders</option>
											<option value="UnassignedWorkOrders" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='UnassignedWorkOrders')?"selected":""?>>Unassigned Work Orders</option>
											<option value="MyOpenWorkOrdersPastDue" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='MyOpenWorkOrdersPastDue')?"selected":""?>>My Open Work Orders Past Due</option>
											<option value="AllOpenWorkOrdersPastDue" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllOpenWorkOrdersPastDue')?"selected":""?>>All Open Work Orders Past Due</option>
											</optgroup>
											<optgroup label="My Custom Views">
												<?php $Views = $this->ViewModel->GetViews();
												foreach ($Views as $View) { ?>
												<option value="<?=$View->WorkOrderViewID?>"><?=$View->WorkOrderViewName?></option>
											<?php } ?>
											</optgroup>
										</select>
										</form>
									</div>
								</div>	
								
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<!-- <div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div> -->
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>WorkOrder/View/CreateNewView"><span>Create New View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<!-- <button class="btn btn-default btn-outline" data-toggle="modal" data-target="#exampleModal" title="Edit Filters"><i class="fa fa-filter"></i></button> -->
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-1 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder" class="btn btn-primary btn-outline">New WorkOrder</a>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="accountglobalfilter" class="form-control accountglobalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">

								<div class="table-responsive">
									<table id="workorders" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">Edit</th>
												<th class="text-center">Assigned To</th>
												<th class="text-center">Work Order No</th>
												<th class="text-center">Account</th>
												<th class="text-center">Subject</th>
												<th class="text-center">Primary Contact</th>
												<th class="text-center">WO Status</th>
												<th class="text-center">WO Priority</th>
												<th class="text-center">Work Order Type</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Grand Total</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									<table id="accountsweek" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">Edit</th>
												<th class="text-center">Assigned To</th>
												<th class="text-center">Work Order No</th>
												<th class="text-center">Account</th>
												<th class="text-center">Subject</th>
												<th class="text-center">Primary Contact</th>
												<th class="text-center">WO Status</th>
												<th class="text-center">WO Priority</th>
												<th class="text-center">Work Order Type</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Grand Total</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Created By</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>	

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form class="form-horizontal" action="<?=SITE_URL?>WorkOrder/View/ViewResult" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">View Filters</h5>
			</div>
			<div class="modal-body">
				
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
						    
						    <?php 
						    /*foreach($WorkOrderFilters as $WorkOrderFilter){ ?>
						    <div class="form-group">
						    	<div class="col-md-3">
									<select class="form-control" name="FilterFields[]">
										<option value="WorkOrderName" <?=$WorkOrderFilter->FilterField=='WorkOrderName'?"Selected":""?> >Ac Name</option>
										<option value="CityName" <?=$WorkOrderFilter->FilterField=='CityName'?"Selected":""?> >City</option>
										<option value="StateName" <?=$WorkOrderFilter->FilterField=='StateName'?"Selected":""?> >State</option>
										<option value="Type" <?=$WorkOrderFilter->FilterField=='Type'?"Selected":""?> >Type</option>
									</select>
								</div>
								<div class="col-md-4">
									<select class="form-control" name="FilterConditions[]">
										<option value="Equals" <?=$WorkOrderFilter->FilterCondition=='Equals'?"Selected":""?> >Equals</option>
										<option value="Contains" <?=$WorkOrderFilter->FilterCondition=='Contains'?"Selected":""?> >Contains</option>
										<option value="StartsWith" <?=$WorkOrderFilter->FilterCondition=='StartsWith'?"Selected":""?> >Starts With</option>
										<option value="DoesNotContain" <?=$WorkOrderFilter->FilterCondition=='DoesNotContain'?"Selected":""?> >Does Not Contain</option>
									</select>
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control" name="FilterValues[]" value="<?=$WorkOrderFilter->FilterValue?>" required>
								</div>
							</div>
							<?php }*/ ?>
							
						</div>
						<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="FilterForm">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>



<?php $this->load->view('Template/FooterView')?>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
 
 	function ViewWorkOrders(flag){ 

 		if(flag != 'MyOpenWorkOrdersToday' && flag != 'MyOpenWorkOrdersThisWeek' && flag != 'AllOpenWorkOrdersToday' && flag != 'AllWorkOrdersToday' && flag != 'AllOpenWorkOrdersThisWeek' && flag != 'WorkOrdersCreatedToday' && flag != 'WorkOrdersCreatedThisWeek' && flag != 'UnscheduledWorkOrders' && flag != 'UnassignedWorkOrders' && flag != 'MyOpenWorkOrdersPastDue' && flag != 'AllOpenWorkOrdersPastDue'){ 
 			$('#SelectedAcForm').submit();
 		}

 		if(flag == 'WorkOrdersCreatedToday' || flag == 'WorkOrdersCreatedThisWeek'){ 

 			var tbl = 'accountsweek';

 			$('#workorders,.workorders').hide();
 			$('#accountsweek,.accountsweek').show();

 			var colarr = [
				{ className: "text-center" },
	            { className: "text-center" },
	            { className: "text-left" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center set-flag" },
	            { className: "text-center set-flag" },
	         ];

 		} else {

 			var tbl = 'workorders';

 			$('#accountsweek,.accountsweek').hide();
 			$('#workorders,.workorders').show();

 			//$(".set-flag").remove();

 			var colarr = [
				{ className: "text-center" },
	            { className: "text-center" },
	            { className: "text-left" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	         ];
	       
 		}

		var dataTable = $('#'+tbl).DataTable({
			 destroy: true,
	       	"responsive": true,
	       	"paging": true,
	       	"bDestroy": true,
	       	"processing": true, //Feature control the processing indicator.
	        "serverSide": true, //Feature control DataTables' server-side processing mode.
	        "sAjaxSource": "<?=SITE_URL?>WorkOrder/View/ViewWorkOrders?flag="+flag,
	       	"columnDefs": [ {
		        sortable: false,
		        targets: 'no-sort',
		        class: "index",
		        targets: 0
	       	} ],
	       	"aaSorting": [],
	       	"order": [[ 1, 'asc' ]],
	       	"fixedColumns": true, 
	       	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
	       	"columns": colarr, 
	        "dom": 'Bfrt<"bottom '+tbl+'"ilp><"clear">',
			buttons: [
				'excel'
			],
			lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'All' ]
			]
	    });
		$('.buttons-excel').hide();
	    $('#accountglobalfilter').keyup(function(){ 
	    	var dataTable = $('#'+tbl).DataTable();
	      	dataTable.search($(this).val()).draw() ;
	    }); 
    }

    $(document).ready(function(){

    	<?php if(isset($SelectedWorkOrder) && !empty($SelectedWorkOrder)){ ?>
    		ViewWorkOrders('<?=$SelectedWorkOrder?>');
    	<?php } else {?>
			ViewWorkOrders('MyOpenWorkOrdersToday');	
    	<?php } ?>
	    
 	});

 	function GoToNewView()
 	{
 		$('#SelectedAcForm').submit();
 	}

 	$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});
</script>



