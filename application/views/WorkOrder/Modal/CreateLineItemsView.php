
<!-- modal --> 
<div class="modal fade" id="create-line-items-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog modal-lg" role="document" style="width: 90%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Line Items</h5>
			</div>
			<form class="form-horizontal" id="CreateWOLineItem" method="post" action="<?=SITE_URL;?>WorkOrder/Actions/WOLineItem/<?=$WorkOrderData->WorkOrderID?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<strong>Product</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>List Price</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Discount</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Unit Price</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Quantity</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Subtotal</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Taxable</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Net Total</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Delete</strong>
						</div>
					</div>

					<?php foreach ($WOLineItems as $key => $WOLineItem) { ?>
						<div class="col-md-12 pt-10 product-line-items">
						<div class="col-md-4">
						<select name="Product[]" id="Product<?=$key?>" class="form-control select2" OnChange="GetProductDetails(this.value, <?=$key?>);">
							<option value="">Select Product</option>
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) { 
								if($WOLineItem->Product == $Product->ProductID) {?>
									<option value="<?=$Product->ProductID?>" selected=""><?=$Product->ProductName?></option>
								<?php } else {?>
									<option value="<?=$Product->ProductID?>"><?=$Product->ProductName?></option>
								<?php } ?>
							<?php } ?>
							</select>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="ListPrice<?=$key?>" name="ListPrice[]" class="form-control" type="text" value="<?=$WOLineItem->ListPrice?>" OnChange="CalculatePriceTotal(<?=$key?>);">
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<input id="Discount<?=$key?>" name="Discount[]" class="form-control" type="text" value="<?=$WOLineItem->Discount?>" OnChange="CalculatePriceTotal(<?=$key?>);"> 
								<span class="input-group-addon">%</span> 
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="UnitPrice<?=$key?>" name="UnitPrice[]" class="form-control" type="text" value="<?=$WOLineItem->UnitPrice?>" OnChange="CalculatePriceTotal(<?=$key?>);" readonly="">
							</div>
						</div>
						<div class="col-md-1">
							<input type="text" name="Quantity[]" id="Quantity<?=$key?>" class="form-control" value="<?=$WOLineItem->Quantity?>" OnChange="CalculatePriceTotal(<?=$key?>);">
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="SubTotal<?=$key?>" name="SubTotal[]" class="form-control" type="text" value="<?=$WOLineItem->SubTotal?>" OnChange="CalculatePriceTotal(<?=$key?>);" readonly="">
							</div>
						</div>
						<div class="col-md-1 text-center pt-10">
							<div class="checkbox checkbox-primary pt-0">
								<input id="Taxable<?=$key?>" name="Taxable[]" type="checkbox" value="1" <?=$WOLineItem->Taxable==1?"checked":"";?> OnChange="CalculatePriceTotal(<?=$key?>);">
								<label for="checkbox2"></label>
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="NetTotal<?=$key?>" name="NetTotal[]" class="form-control NetTotal" type="text" value="<?=$WOLineItem->NetTotal?>" OnChange="CalculatePriceTotal(<?=$key?>);" readonly="">
							</div>
						</div>
						<div class="col-md-1 text-center">
							<a href="#" class="btn btn-default remove_line_item_field"><i class="fa fa-trash-o"></i></a>
						</div>
					</div>
					<?php } ?> 

					<!-- <div class="col-md-12 pt-10">
						<div class="col-md-4">
						<select name="Product[]" id="Product" class="form-control select2" required="">
							<option value="">Select Product</option>
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
									<option value="<?=$Product->ProductID?>"><?=$Product->ProductName?></option>
							<?php } ?>
							</select>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="ListPrice" name="ListPrice[]" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<input id="Discount" name="Discount[]" class="form-control" type="text">
								<span class="input-group-addon">%</span> 
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="UnitPrice" name="UnitPrice[]" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-1">
							<input type="text" name="Quantity[]" id="Quantity" class="form-control">
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="SubTotal" name="SubTotal[]" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-1 text-center pt-10">
							<div class="checkbox checkbox-primary pt-0">
								<input id="Taxable" name="Taxable[]" type="checkbox" value="1">
								<label for="checkbox2"></label>
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group"> 
								<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								<input id="NetTotal" name="NetTotal[]" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-1 text-center">
							<a href="#" class="btn btn-default remove_line_item_field"><i class="fa fa-trash-o"></i></a>
						</div>
					</div> -->

					<div class="line_item_fields_wrap">
						
					</div>
					<div class="col-md-12 pt-10">
						<div class="col-md-10"><strong class="pull-right"><label class="control-label"> Grand Total</label></strong></div>
						<div class="col-md-1">
						<div class="input-group"> 
							<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
							<input id="GrandTotal" name="GrandTotal" class="form-control" type="text">
						</div>
						</div>
					</div>
					<div class="col-md-12">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15 add_line_item_field_button"><i class="fa fa-plus"></i> Add Line Item</a>
					</div>
					
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateNote">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

