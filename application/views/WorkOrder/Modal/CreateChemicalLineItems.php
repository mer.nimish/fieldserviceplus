
<!-- modal --> 
<div class="modal fade" id="create-chemical-line-items-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog modal-lg" role="document" style="width: 70%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Line Items</h5>
			</div>
			<form class="form-horizontal" id="CreateWOLineItem" method="post" action="<?=SITE_URL;?>WorkOrder/Actions/WOChemicalLineItems/<?=$WorkOrderData->WorkOrderID?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<strong>Chemical</strong>
						</div>
						<div class="col-md-4 text-center">
							<strong>Tested Concentration</strong>
						</div>
						<div class="col-md-3 text-center">
							<strong>Application</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Delete</strong>
						</div>
					</div>

					<?php foreach ($WOChemicalLineItems as $key => $WOChemicalLineItem) { ?>

						<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
								 <div class="panel panel-default">
								 	<div class="panel-heading activestate col-md-1 pt-5" role="tab" id="heading_10" style="width:2%;border-bottom: none;">
										<a role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_10" aria-expanded="true" ><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div></a> 
									</div> 

									<div class="col-md-11 pt-10 product-line-items">
										<div class="col-md-4">
										<select name="Chemical[]" id="Chemical<?=$key?>" class="form-control select2">
											<option value="">Select Chemical</option>
											<?php $Chemicals = $this->ActionsModel->GetChemicals();
												foreach ($Chemicals as $Chemical) { 
												if($WOChemicalLineItem->Chemical == $Chemical->ChemicalID) {?>
													<option value="<?=$Chemical->ChemicalID?>" selected=""><?=$Chemical->ChemicalName?></option>
												<?php } else {?>
													<option value="<?=$Chemical->ChemicalID?>"><?=$Chemical->ChemicalName?></option>
												<?php } ?>
											<?php } ?>
											</select>
										</div>
										<div class="col-md-4">
											<div class="col-md-2"></div>
											<div class="col-md-3"> 
												<input id="ListPrice<?=$key?>" name="ListPrice[]" class="form-control" type="text">
											</div>
											<div class="col-md-5"> 
												<select name="Chemical[]" id="Chemical<?=$key?>" class="form-control">
													<option value="">Select</option>
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="col-md-2"></div>
											<div class="col-md-3"> 
												<input id="ListPrice<?=$key?>" name="ListPrice[]" class="form-control" type="text">
											</div>
											<div class="col-md-5"> 
												<select name="Chemical[]" id="Chemical<?=$key?>" class="form-control">
													<option value="">Select</option>
												</select>
											</div>
										</div>
										<div class="col-md-1 text-center">
											<a href="#" class="btn btn-default remove_line_item_field"><i class="fa fa-trash-o"></i></a>
										</div>
									</div>

									<div id="collapse_10" class="panel-collapse collapse in col-md-12" role="tabpanel">
										<div class="panel-body pa-15"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer la. </div>
									</div>
								</div> 
							</div>

						
					<?php } ?> 

					<div class="line_item_fields_wrap">
						
					</div>
					<div class="col-md-12">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15 add_line_item_field_button"><i class="fa fa-plus"></i> Add Line Item</a>
					</div>
					
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateNote">Save</button>
			</div>
			</form>
		</div>
	</div>
</div> 

