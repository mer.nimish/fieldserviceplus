
<?php $this->load->view('Template/HeaderView')?>
<!-- Bootstrap Datetimepicker CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
 <!--Signature Pad CSS -->
<link href="<?=SITE_URL?>dist/css/signature_pad.css" rel="stylesheet" type="text/css">


<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Work Order</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder" class="form-horizontal" id="CreateWorkOrder" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Work Order Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Assigned To</label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers(); 
															foreach ($Users as $User) { 
															if(isset($WorkOrderData->AssignedTo) && $WorkOrderData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Account</label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if(isset($WorkOrderData->Account) && $WorkOrderData->Account == $Account->AccountID){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName;?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Primary Contact</label>
														<div class="col-md-9">

														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="PrimaryContact" id="PrimaryContact">
															<option value="">Select Contact</option>
															<?php /*foreach ($Contacts as $Contact) { 
																if(isset($WorkOrderData->PrimaryContact) && $WorkOrderData->PrimaryContact == $Contact->ContactID){ ?>
																	<option value="<?=$Contact->ContactID?>" selected="selected"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } ?>
															<?php }*/ ?>
															<?php echo form_error('PrimaryContact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Parent Work Order</label>
														<div class="col-md-9">

														<?php $GetWorkOrders = $this->ActionsModel->GetWorkOrders(); ?>
															<select class="form-control select2" name="ParentWorkOrder" id="ParentWorkOrder">
															<option value="">Select Work Order</option>
															<?php foreach ($GetWorkOrders as $GetWorkOrder) { 
																if(isset($WorkOrderData->ParentWorkOrder) && $WorkOrderData->ParentWorkOrder == $GetWorkOrder->WorkOrderID){ ?>
																	<option value="<?=$GetWorkOrder->WorkOrderID?>" selected="selected"><?=$GetWorkOrder->Subject?></option>
																<?php } else { ?>
																	<option value="<?=$GetWorkOrder->WorkOrderID?>"><?=$GetWorkOrder->Subject?></option>
																<?php } ?>
															<?php } ?>
															<?php echo form_error('ParentWorkOrder');  ?>
														</select>
														</div>
													</div>

												
													<div class="form-group">
														<label class="control-label col-md-3">Subject</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=isset($WorkOrderData->Subject)?$WorkOrderData->Subject:"";?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Description</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($WorkOrderData->Description)?$WorkOrderData->Description:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Pop Up Reminder</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PopUpReminder" id="PopUpReminder" value="<?=isset($WorkOrderData->PopUpReminder)?$WorkOrderData->PopUpReminder:"";?>">
															<?php echo form_error('PopUpReminder');  ?>
														</div>
													</div>
												</div>


												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">WorkOrder Type</label>
														<div class="col-md-9">

														<?php $WorkOrderTypes = $this->ActionsModel->GetWorkOrderTypes(); ?>
															<select class="form-control" name="WorkOrderType" id="WorkOrderType">
															<option value="">Select Work Order Type</option>
															<?php foreach ($WorkOrderTypes as $WorkOrderType) { 
																if(isset($WorkOrderData->WorkOrderType) && $WorkOrderData->WorkOrderType == $WorkOrderType->WorkOrderType){ ?>
																	<option value="<?=$WorkOrderType->WorkOrderTypeID?>" selected="selected"><?=$WorkOrderType->WorkOrderType?></option>
																<?php } else { ?>
																	<option value="<?=$WorkOrderType->WorkOrderTypeID?>"><?=$WorkOrderType->WorkOrderType?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WorkOrderType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Status</label>
														<div class="col-md-9">

														<?php $GetWOStatus = $this->ActionsModel->GetWOStatus(); ?>
															<select class="form-control" name="WOStatus" id="WOStatus">
															<option value="">Select Status</option>
															<?php foreach ($GetWOStatus as $WOStatus) { 
																if(isset($WorkOrderData->WOStatus) && $WorkOrderData->WOStatus == $WOStatus->WOStatusID){ ?>
																	<option value="<?=$WOStatus->WOStatusID?>" selected="selected"><?=$WOStatus->Status?></option>
																<?php } else { ?>
																	<option value="<?=$WOStatus->WOStatusID?>"><?=$WOStatus->Status?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WOStatus');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Priority</label>
														<div class="col-md-9">

														<?php $WOPriorities = $this->ActionsModel->WOPriorities(); ?>
															<select class="form-control" name="WOPriority" id="WOPriority">
															<option value="">Select Priority</option>
															<?php foreach ($WOPriorities as $WOPriority) { 
																if(isset($WorkOrderData->WOPriority) && $WorkOrderData->WOPriority == $WOPriority->WOPriorityID){ ?>
																	<option value="<?=$WOPriority->WOPriorityID?>" selected="selected"><?=$WOPriority->Priority?></option>
																<?php } else { ?>
																<option value="<?=$WOPriority->WOPriorityID?>"><?=$WOPriority->Priority?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WOPriority');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Category</label>
														<div class="col-md-9">

														<?php $WOCategories = $this->ActionsModel->WOCategories(); ?>
															<select class="form-control" name="WOCategory" id="WOCategory">
															<option value="">Select Category</option>
															<?php foreach ($WOCategories as $WOCategory) { 
																if(isset($WorkOrderData->WOCategory) && $WorkOrderData->WOCategory == $WOCategory->WOCategoryID){ ?>
																	<option value="<?=$WOCategory->WOCategoryID?>" selected="selected"><?=$WOCategory->CategoryName?></option>
																<?php } else { ?>
																	<option value="<?=$WOCategory->WOCategoryID?>"><?=$WOCategory->CategoryName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WOCategory');  ?>
														</div>
													</div>


												</div>
											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Scheduling and Address Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Start Date Time</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatetimepicker" name="StartDateTime" id="StartDateTime" value="<?=isset($WorkOrderData->StartDate)?$WorkOrderData->StartDate:"";?>">
															<?php echo form_error('StartDateTime');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">End Date Time</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatetimepicker" name="EndDateTime" id="EndDateTime" value="<?=isset($WorkOrderData->EndDate)?$WorkOrderData->EndDate:"";?>">
															<?php echo form_error('EndDateTime');  ?>
														</div>
													</div>

													
													<div class="form-group">
														<label class="control-label col-md-3">Is Recurring?</label>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary">
																<input id="IsRecurring" name="IsRecurring" type="checkbox" value="1">
																<label for="checkbox2"></label>
																<input id="WORecurrenceID" name="WORecurrenceID" class="WORecurrenceID" type="hidden">
															</div>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Address</label>
														<div class="col-md-9">
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label">Street</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Address" id="Address" placeholder="" value="<?=isset($WorkOrderData->Address)?$WorkOrderData->Address:"";?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($WorkOrderData->Latitude)?$WorkOrderData->Latitude:"";?>">
															<input type="hidden" name="Longitude" id="Longitude" value="<?=isset($WorkOrderData->Longitude)?$WorkOrderData->Longitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">City</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="City" id="City" value="<?=isset($WorkOrderData->City)?$WorkOrderData->City:"";?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">State</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="State" id="State" value="<?=isset($WorkOrderData->State)?$WorkOrderData->State:"";?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Country</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=isset($WorkOrderData->Country)?$WorkOrderData->Country:"";?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Postal Code</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=isset($WorkOrderData->PostalCode)?$WorkOrderData->PostalCode:"";?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>

												</div>
												<!--/span-->


											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Sub Total</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="SubTotal" id="SubTotal" value="<?=isset($WorkOrderData->SubTotal)?$WorkOrderData->SubTotal:"";?>">
															<?php echo form_error('SubTotal');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Discount</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Discount" id="Discount" value="<?=isset($WorkOrderData->Discount)?$WorkOrderData->Discount:"";?>">
															<?php echo form_error('Discount');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Tax</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Tax" id="Tax" value="<?=isset($WorkOrderData->Tax)?$WorkOrderData->Tax:"";?>">
															<?php echo form_error('Tax');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Total Price</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="TotalPrice" id="TotalPrice" value="<?=isset($WorkOrderData->TotalPrice)?$WorkOrderData->TotalPrice:"";?>">
															<?php echo form_error('TotalPrice');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Grand Total</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="GrandTotal" id="GrandTotal" value="<?=isset($WorkOrderData->GrandTotal)?$WorkOrderData->GrandTotal:"";?>">
															<?php echo form_error('GrandTotal');  ?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Signature</label>
														<!-- <div class="col-md-9">
															<input type="text" class="form-control" name="Signature" id="Signature" value="<?=isset($WorkOrderData->Signature)?$WorkOrderData->Signature:"";?>">
															<?php echo form_error('Signature');  ?>
														</div> -->
														<div class="col-md-4">
														 <div id="signature-pad-1" class="m-signature-pad">
				                                            <div class="m-signature-pad--body">
				                                                <canvas></canvas> 
				                                            </div>
				                                            <input type="hidden" name="Signature" id="Signature"> 
				                                        </div>
				                                        </div>
				                                        <div class="col-md-1 pull-left">
				                                        	 <button type="button" class="btn btn-default btn-outline" data-action="clear1"><i class="fa fa-eraser"></i></button>
				                                        </div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Line Item Count</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="LineItemCount" id="LineItemCount" value="<?=isset($WorkOrderData->LineItemCount)?$WorkOrderData->LineItemCount:"";?>">
															<?php echo form_error('LineItemCount');  ?>
														</div>
													</div>
												</div>

											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateWorkOrder">Save</button>
															<a href="<?=SITE_URL;?>WorkOrder/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
<!-- modal -->
<!-- <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="popupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="Location" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> -->

<?php $this->load->view('WorkOrder/Modal/LocationPickerView'); ?>
<?php $this->load->view('WorkOrder/Modal/RecurringTaskView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Signature Pad JavaScript Start -->
<script type="text/javascript" src="<?=SITE_URL?>dist/js/signature_pad.js"></script>
<script type="text/javascript">
var wrapper1 = document.getElementById("signature-pad-1"),
        canvas1 = wrapper1.querySelector("canvas"),
        signaturePad1;

function resizeCanvas(canvas) {
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

resizeCanvas(canvas1);
signaturePad1 = new SignaturePad(canvas1);
canvas1.addEventListener('mouseup', function (e) {
    document.getElementById("Signature").value = signaturePad1.toDataURL();
});
var clearButton1 = document.querySelector("[data-action=clear1]");
clearButton1.addEventListener("click", function (event) {
    signaturePad1.clear();
    document.getElementById("Signature").value = '';
});

function setSignImg(){
  if (!signaturePad1.isEmpty()) { 
       document.getElementById("Signature").value = signaturePad1.toDataURL();
  }
} 
</script>
<!-- Signature Pad JavaScript End -->

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(".vertical-spin").TouchSpin({
		min: 1,
        max: 150,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
	var vspinTrue = $(".vertical-spin").TouchSpin({
		verticalbuttons: true
	});
	if (vspinTrue) {
		$('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
	}

	$(".vertical-spin-occurrence").TouchSpin({
		min: 1,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});

	/* Recurring Setting */
function RepeatsChange(Repeats){ alert();
	if(Repeats == 'Daily'){ 
			
		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('day on');
	} else if(Repeats == 'Weekly'){ 

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 104});
	
		$( "#days-cb" ).show();
		$("input[name*='RepeatOn[]']").attr("required",true);
		$('.interval_every_txt').html('week on');
	} else if(Repeats == 'Monthly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 60});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('month on');
	} else if(Repeats == 'Yearly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 10});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('year on');
	} else if(Repeats == 'Periodically'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('periodical on');
	}
}
$("input[name='Ends']").change(function() { 
	if($("input[name='Ends']:checked").val()=='On') {
       $("#EndsOnDate").attr("required",true);
 	} else {
 		$("#EndsOnDate").attr("required",false);
 	}
}); 

$('#IsRecurring').change(function() { 
     if(this.checked) {
        $('#RecurringModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
         
     } else {

     	if($('.WORecurrenceID').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceID').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Account/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceID').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#RecurringForm').validate({
	errorPlacement: function(){
	    return false;
	},
	/* submitHandler: function (form) { 
	    return false; 
	},*/
	highlight: function(element) {
		$(element).closest('.form-group').addClass('has-error');
	},
	unhighlight: function(element) {
	    $(element).closest('.form-group').removeClass('has-error');
	}
});	

$(document).on('click', "#RecurrenceSave", function () {
    if ($("#RecurringForm").valid()) {	
        $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/saveRecurrenceSetting/',
            data: $("#RecurringForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                   	$('.WORecurrenceID').val(result); 
                   
                   	$('#RecurringModal').modal('hide'); 
                   	return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
    }
    return false;
});

$(document).on('click', "#RecurrenceClose", function () { 
   	$('#IsRecurring').prop('checked', false);
});
</script>


<script type="text/javascript">

$(document).ready(function () {
 
    $('#CreateWorkOrder').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Account: {
                required: true
            },
            ParentWorkOrder: {
                required: true
            },
            Subject: {
                required: true
            },
            Description: {
                required: true
            },
            Address: {
                required: true
            },
            Street: {
                required: true
            },
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            WorkOrderType: {
                required: true
            },
            WOStatus: {
                required: true
            },
            WOPriority: {
                required: true
            },
            WOCategory: {
                required: true
            },
           /* PopUpReminder: {
                required: true
            },*/
            StartDateTime: {
                required: true
            },
            EndDateTime: {
                required: true
            },
            PrimaryContact: {
                required: true
            },
            GrandTotal: {
                required: true
            },
            LineItemCount: {
                required: true
            },
            SubTotal: {
                required: true
            },
            Tax: {
                required: true
            },
            TotalPrice: {
                required: true
            },
            Signature: {
                required: true
            },
            Discount: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        	setSignImg();

        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /*$(".datepicker").datepicker();*/
     /*$('.datepicker').datepicker({
	    format: 'mm/dd/yyyy',
	    startDate: '-3d'
	});*/

    $('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
		});

	$('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
		}).data("DateTimePicker").date(moment());

});
</script>

<script type="text/javascript">
function GetPrimaryContactOptions(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	set_location_in_map();
});
</script>