<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark"><?=$WorkOrderData->Subject;?></h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<div class="form-group">
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							<!-- <ul role="tablist" class="nav nav-pills pull-right" id="myTabs_6">
								<li><a class="btn-outline btn btn-primary" href="<?=SITE_URL?>WorkOrder/Actions/EditWorkOrder/<?=$WorkOrderData->WorkOrderID;?>">Edit</a></li>
							<li class="custom_li_btn"><a data-toggle="tab" href="#">Copy</a></li> 
								<li>
									<select class="selectpicker" data-style="form-control btn-default btn-outline">
										<option disabled="" selected="">Actions</option>
										<optgroup label="Add New Actions">
											<option>New Work Order</option>
											<option>New Estimate</option>
											<option>New Contact</option>
											<option>New Location</option>
											<option>New Invoice</option>
											<option>New File</option>
										</optgroup>
										<optgroup label="WorkOrder Actions">
											<option OnClick="CopyWorkOrder()">Copy WorkOrder</option>
											<option>Delete WorkOrder</option>
										</optgroup>
									</select>

								</li>
							</ul> -->
							
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li>
												<a href="#"><span>Send Text</span></a>
											</li>
											<li>
												<a href="#"><span>Send Email</span></a>
											</li>
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="#" class="add_new_scheduling_line"><span>Schedule a Time</span></a>
											</li>
											<li>
												<a href="#" class="add_new_scheduling_line"><span>New Line Item</span></a>
											</li>
											<li>
												<a href="#"><span>New Chemical</span></a>
											</li>
											<li>
												<a href="#"><span>New File</span></a>
											</li>
											<li>
												<a href="#"><span>New Task</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-note-modal"><span>New Note</span></a>
											</li>
											<li>
												<a href="#"><span>New Invoice</span></a>
											</li>
											<li>
												<a href="#"><span>Convert to Invoice</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>WorkOrder Actions</strong></span></a>
											</li>
											<!-- <li> 
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder"><span>Create Recurring Work Order</span></a>
											</li> -->
											<li> 
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder/<?=$WorkOrderData->WorkOrderID?>/"><span>Copy WorkOrder</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteWorkOrder();"><span>Delete WorkOrder</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>WorkOrder/Actions/EditWorkOrder/<?=$WorkOrderData->WorkOrderID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">WorkOrder Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>WorkOrder/Actions/EditWorkOrder/<?=$WorkOrderData->WorkOrderID?>" class="form-horizontal" id="EditWorkOrder" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>WorkOrder Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
												
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Work Order No
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->WorkOrderNo;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Assigned To
														</div>
														<div class="col-md-8">
															<a href=""><?=$WorkOrderData->AssignedToName;?></a>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Account
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$WorkOrderData->Account?>"><?=$WorkOrderData->AccountName;?></a>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Primary Contact
														</div>
														<div class="col-md-8">
															<a href=""><?=$WorkOrderData->PrimaryContactName;?></a>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Parent Work Order
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrderData->ParentWorkOrder?>"><?=$WorkOrderData->ParentWorkOrderName;?></a>
														</div>
													</div>
												
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Subject
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Subject;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Description
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Description;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Pop Up Reminder
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->PopUpReminder;?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Type
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->WorkOrderTypeName;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Status
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Status;?>
														</div>
													</div>


													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Priority
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Priority;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Category
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->CategoryName;?>
														</div>
													</div>
											</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">

												<!-- <div class="col-md-6">	

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Start Date Time
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->StartDate;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															End Date Time
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->EndDate;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Is Recurring?
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0">
																<input id="IsRecurring" name="IsRecurring" type="checkbox" value="1" <?=($WorkOrderData->IsRecurring==1)?"checked=''":""?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

												</div> -->
												<div class="col-md-6">	

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Address
														</div>
														<div class="col-md-8">
															<div><?=$WorkOrderData->Address;?></div>
															<div><?=$WorkOrderData->City;?>, <?=$WorkOrderData->State;?> <?=$WorkOrderData->PostalCode;?></div>
															<div><?=$WorkOrderData->Country;?></div>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Sub Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$WorkOrderData->SubTotal, 2, '.', '');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Discount
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Discount;?>%
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Tax
														</div>
														<div class="col-md-8">
															<?=number_format((double)$WorkOrderData->Tax, 2, '.', '');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Total Price
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$WorkOrderData->TotalPrice, 2, '.', '');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Grand Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$WorkOrderData->GrandTotal, 2, '.', '');?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Signature
														</div>
														<div class="col-md-8">
														<?php if(!empty($WorkOrderData->Signature)){?>
															 <img src="<?=$WorkOrderData->Signature;?>" />
														<?php } ?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Line Item Cnt
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->LineItemCount;?>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="#"><?=$WorkOrderData->CreatedBy;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="#"><?=$WorkOrderData->LastModifiedBy;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php $this->load->view('WorkOrder/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php $this->load->view('WorkOrder/Include/SchedulingAndNotes'); ?>
			
		</div>

	</div>
	<!-- /Row -->

</div>


<!-- Start Modal -->
<?php //$this->load->view('WorkOrder/Modal/CreateTaskView'); ?>

<?php //$this->load->view('WorkOrder/Modal/CreateEventView'); ?>

<?php //$this->load->view('WorkOrder/Modal/RecurringTaskView'); ?>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

<?php $this->load->view('WorkOrder/Modal/CreateLineItemsView'); ?>
<?php $this->load->view('WorkOrder/Modal/CreateChemicalLineItems'); ?>
<?php $this->load->view('WorkOrder/Modal/CreateNoteView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$(".select2").select2();
});
</script>

<script type="text/javascript">

$(document).ready(function () {

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

   /* $('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
	}).data("DateTimePicker").date(moment());*/

    $('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
}); 
function DeleteWorkOrder(WorkOrderID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#f8b32d",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>WorkOrder/Actions/DeleteWorkOrder/<?=$WorkOrderData->WorkOrderID;?>/";
		}, 500);
    });
	return false;
}





/* Scheduling Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".new_scheduling_line_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_new_scheduling_line").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="SchedulingLine'+x+'"><div class="form-wrap">'
					+	'<form action="#" class="form-horizontal" id="SchedulingForm">'
					+   '<input type="hidden" name="Subject" value="<?=$WorkOrderData->Subject;?>">'
					+   '<input type="hidden" name="WorkOrderID" value="<?=$WorkOrderData->WorkOrderID;?>">'
					+   '<input type="hidden" name="Who" value="<?=$WorkOrderData->PrimaryContact;?>">'
					+   '<input type="hidden" name="Description" value="<?=$WorkOrderData->Description;?>">'
					+	'<div class="form-body">'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Assigned To</label>'
					+			'<div class="col-md-6">'
					+				'<select name="AssignedTo" id="AssignedTo" class="form-control select2">'
					+				'<option value="">Select Owner</option>'
									<?php $Users = $this->ActionsModel->GetUsers();
										foreach ($Users as $User) { 
											if($WorkOrderData->AssignedTo == $User->UserID){?>
					+							'<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php } else {?>
					+							'<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php }
										} ?>
					+				'</select>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Start</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="<?=date("m/d/Y h:i A")?>" required>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">End</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="<?=date("m/d/Y h:i A")?>" required>'
					+			'</div>'
					+		'</div>'
					+	'</div>'
					+	'<div class="pull-right">'
					+		'<button type="button" class="btn btn-default remove_new_scheduling_line" data-dismiss="modal">Close</button>'
					+		'<button type="button" class="btn btn-primary" OnClick="SaveScheduling('+x+')">Save</button>'
					+	'</div>'
					+	'</form>'
					+ '<div class="clearfix"></div>'
					+ '<hr class="light-grey-hr">'
					+ '</div>'
					+ '</div>'); //add input box
		}
		$(".select2").select2();
		$('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
	                time: "fa fa-clock-o",
	                date: "fa fa-calendar",
	                up: "fa fa-arrow-up",
	                down: "fa fa-arrow-down"
	            }
		});
    });
    $(wrapper).on("click",".remove_new_scheduling_line", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-wrap').remove(); x--;
    })
});


function SaveScheduling(x)
{
 	 $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/SaveScheduling/',
            data: $("#SchedulingForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	$("#SchedulingLine"+x).html(result);
                   	return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
}

function EditScheduling(EventID)
{
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetScheduling/'+EventID,
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {

            	$("#Event"+EventID).html(result); //add input box
            	$(".select2").select2();
				$('.showdatetimepicker').datetimepicker({
					useCurrent: true,
					icons: {
			                time: "fa fa-clock-o",
			                date: "fa fa-calendar",
			                up: "fa fa-arrow-up",
			                down: "fa fa-arrow-down"
			            }
				});
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}	

function UpdateScheduling(EventID)
{
 	 $.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/UpdateScheduling/'+EventID,
        data: $("#SchedulingForm"+EventID).serialize(),
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#Event"+EventID).html(result);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function DeleteScheduling(EventID)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/DeleteScheduling/'+EventID,
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#Event"+EventID).html('');
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function EditSchAssignedTo(EventID, AssignedTo)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/EditSchAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="UpdateAssignedTo('+EventID+')">Save</a>');
            	$(".select2").select2();
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function UpdateAssignedTo(EventID)
{ 
	var AssignedTo = $("#AssignedToVal"+EventID).val();
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/UpdateAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="EditSchAssignedTo('+EventID+', '+AssignedTo+')">Change</a>');
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

/* Scheduling End */ 
</script>


<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($WOLineItems)-1;?>; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="form-control select2" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Product</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<div class="input-group">' 
					+			'<span class="input-group-addon"><i class="fa fa-dollar"></i></span>'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<div class="input-group">' 
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					+			'<span class="input-group-addon">%</span>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<div class="input-group">' 
					+			'<span class="input-group-addon"><i class="fa fa-dollar"></i></span>'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<div class="input-group">' 
					+			'<span class="input-group-addon"><i class="fa fa-dollar"></i></span>'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<div class="input-group">'
					+			'<span class="input-group-addon"><i class="fa fa-dollar"></i></span>'
					+			'<input id="NetTotal'+x+'" name="NetTotal[]" class="form-control NetTotal" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		$(".select2").select2();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}
            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val());
	var Discount = parseFloat($('#Discount'+x).val());
	var Quantity = parseFloat($('#Quantity'+x).val());

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var NetTotal = SubTotal + (SubTotal * <?=MY_Model::getTax();?>) / 100;
	else 
		var NetTotal = SubTotal;
	 
	$('#UnitPrice'+x).val(UnitPrice);
	$('#SubTotal'+x).val(SubTotal);
	$('#NetTotal'+x).val(NetTotal);

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;
    $(".NetTotal").each(function(){
        GrandTotal += +$(this).val();
    });
	$('#GrandTotal').val(GrandTotal);
}

/* Line Items End */ 
</script>


