
<?php $this->load->view('Template/HeaderView')?>
<!-- multi-select CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>

<!-- Bootstrap Switches CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Edit View</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Account/View/EditCustomView/<?=$AccountViewData->AccountViewID?>" class="form-horizontal" id="CreateNewView" method="post">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Enter View Name</div>
													<hr class="light-grey-hr"/>

													<div class="form-group">
														<label class="control-label col-md-2">View Name</label>
														<div class="col-md-4">
															<input type="text" name="AccountViewName" class="form-control" placeholder="Enter View Name Here" value="<?=$AccountViewData->AccountViewName?>">
														</div>
													</div>

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Specify Filter Criteria</div>
													<hr class="light-grey-hr"/>
												
													<div class="col-md-12 pb-10">Enter Additional Filters Criteria (optional):</div>

													<?php foreach ($AccountFilterData as $key => $value) { ?>
														
														<div class="form-group">
															<div class="col-md-1"></div>
															<div class="col-md-2">
																<select class="form-control" name="FilterFields[]">
																<?php $Fields = $this->ViewModel->AccountViewFields(); 

																foreach($Fields as $Field){ 

																	if($value->FilterField == $Field->FieldName){?>

																		<option value="<?=$Field->FieldName;?>" selected=""><?=$Field->FieldName;?></option>
																	<?php } else {?>
																		<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																	<?php } ?>
																<?php } ?>
																	
																</select>
															</div>
															<div class="col-md-2">
																<select class="form-control" name="FilterConditions[]">
																	<option value="Equals" <?=($value->FilterCondition == "Equals")?"selected=''":"";?>>Equals</option>
																	<option value="Contains" <?=($value->FilterCondition == "Contains")?"selected=''":"";?>>Contains</option>
																	<option value="StartsWith" <?=($value->FilterCondition == "StartsWith")?"selected=''":"";?>>Starts With</option>
																	<option value="DoesNotContain" <?=($value->FilterCondition == "DoesNotContain")?"selected=''":"";?>>Does Not Contain</option>
																</select>
															</div>
															<div class="col-md-3">
																<input type="text" class="form-control" name="FilterValues[]" value="<?=$value->FilterValue?>">
															</div>
														</div>

													<?php } ?>

													<div class="input_fields_wrap" >
													</div>

													<div class="col-md-12 pb-15">
														<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
													</div>

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Specify Fields to Display</div>
													<hr class="light-grey-hr"/>
													<div class="col-md-12">
														<div class="col-md-2"></div>
														<div class="col-md-6 pb-15">
															<select multiple="multiple" id="my-select" name="SpecifyFieldsDisplay[]">
														      	<?php $SpecifyFieldsDisplay = explode(",", $AccountViewData->SpecifyFieldsDisplay); 
														      	$Fields = $this->ViewModel->AccountViewFields(); 

																foreach($Fields as $Field){ 

																	if(in_array($Field->FieldName, $SpecifyFieldsDisplay)){?>

																		<option value="<?=$Field->FieldName;?>" selected=""><?=$Field->FieldName;?></option>
																	<?php } else {?>
																		<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																	<?php } ?>
																<?php } ?>
														    </select>
														</div>
														<div class="col-md-4"></div>
													</div>

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Restrict Visibility</div>
													<hr class="light-grey-hr"/>
													<div class="col-md-12">
														<div class="form-group" >
															<div class="col-sm-12">
																<div class="radio radio-primary">
																	<input name="RestrictVisibility" id="radio11" value="VisibleOnlyToMe" <?=($AccountViewData->RestrictVisibility == "VisibleOnlyToMe")?"checked=''":"";?> type="radio" >
																	<label for="radio11"> Visible only to me </label>
																</div>
																<div class="radio radio-primary">
																	<input name="RestrictVisibility" id="radio12" value="VisibleToEveryone" type="radio" <?=($AccountViewData->RestrictVisibility == "VisibleToEveryone")?"checked=''":"";?>>
																	<label for="radio12"> Visible to everyone </label>
																</div>
															</div>
														</div>
													</div>

												</div>
												<!--/span-->

											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditCustomView">Save</button>
															<button type="button" class="btn btn-default">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Multiselect JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>
<!-- Bootstrap Switch JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#CreateNewView').validate({ 
        rules: {
            AccountViewName: {
                required: true
            },
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});

</script>

<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group" >'
					+'<div class="col-md-1"></div>'
					+'<div class="col-md-2">'
					+'	<select class="form-control" name="FilterFields[]">'
					+'		<option value="AccountType">Account Type</option>'
					+'		<option value="AccountName">Account Name</option>'
					+'		<option value="AssignedTo">Assigned To</option>'
					+'		<option value="PreferredTechnician">Preferred Technician</option>'
					+'		<option value="PrimaryContact">Primary Contact</option>'
					+'		<option value="BillingAddress">Billing Address</option>'
					+'		<option value="BillingCity">Billing City</option>'
					+'		<option value="BillingState">Billing State</option>'
					+'		<option value="BillingCountry">Billing Country</option>'
					+'		<option value="BillingPostalCode">Billing Postal Code</option>'
					+'		<option value="Description">Description</option>'
					+'		<option value="LastServiceDate">Last Service Date</option>'
					+'		<option value="LastActivityDate">Last Activity Date</option>'
					+'		<option value="Notes">Notes</option>'
					+'		<option value="PhoneNo">Phone No</option>'
					+'		<option value="ShippingAddress">Shipping Address</option>'
					+'		<option value="Notes">Notes</option>'
					+'		<option value="ShippingCity">Shipping City</option>'
					+'		<option value="ShippingState">Shipping State</option>'
					+'		<option value="ShippingCountry">Shipping Country</option>'
					+'		<option value="ShippingPostalCode">Shipping PostalCode</option>'
					+'		<option value="AccessNotes">Access Notes</option>'
					+'		<option value="Notes">Notes</option>'
					+'		<option value="PopUpReminder">PopUp Reminder</option>'
					+'	</select>'
					+'</div>'
					+'<div class="col-md-2">'
					+'		<select class="form-control" name="FilterConditions[]">'
					+'	<option value="Equals">Equals</option>'
					+'	<option value="Contains">Contains</option>'
					+'	<option value="StartsWith">Starts With</option>'
					+'	<option value="DoesNotContain">Does Not Contain</option>'
					+'</select>'
					+'</div>'
					+'<div class="col-md-3">'
					+'	<input type="text" class="form-control" name="FilterValues[]">'
					+'</div>'
				+'</div>'); //add input box
		}
    });
   
   /* $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })*/
});

/* Multiselect Init*/     
$('#my-select').multiSelect({ selectableOptgroup: true, selectableHeader: '<div style="text-align: center;">Available Fields</div>', selectionHeader: '<div style="text-align: center;">Displayed Fields</div>' });

</script>
