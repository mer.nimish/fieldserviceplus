<div class="panel panel-default card-view">
	 <div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Related Lists / Information</h6>
		</div>
		<div class="clearfix"></div>
	</div> 
	<div class="panel-wrapper collapse in">
		<div class="panel-body">

		<div class="row">

			<div class="col-md-12">
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0 pb-10">
						<h5 class="txt-dark mt-20"><small>Work Order Line Item</small> </h5>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="table-wrap">
				<div class="table-responsive">
				<table id="ContactsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>WO Line # </th>
							<th>Product</th>
							<th>Unit Price</th>
							<th>Quantity</th>
							<th>Net Total</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($WOLineItems as $key => $WorkOrderLineItem) { ?>
						<tr>
							<td><?=$WorkOrderLineItem->LineItemNo?></td>
							<td><a href="#"><?=$WorkOrderLineItem->ProductName?></a></td>
							<td><?=$WorkOrderLineItem->UnitPrice?></td>
							<td><?=$WorkOrderLineItem->Quantity?></td>
							<td><?=$WorkOrderLineItem->NetTotal?></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15 add-new-line-item"  data-toggle="modal" data-target="#create-line-items-modal" ><i class="fa fa-plus"></i> Add Line Item</a>
						<button type="button" class="btn btn-primary btn-outline mb-10 mt-15 edit-line-items" data-toggle="modal" data-target="#create-line-items-modal"> Edit Lines</button>
				</div>
				<?php if(count($WOLineItems) >= 10){?>
					<div class="pull-right">
						<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/WOLineItems" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
					</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0 pb-10">
						<h5 class="txt-dark mt-20"><small>Chemicals</small> </h5>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="table-wrap">
				<div class="table-responsive">
				<table id="ContactsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>CH-#</th>
							<th>Product</th>
							<th>Measure</th>
							<th>Unit of Measure</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($WOChemicalLineItems as $key => $WOChemicalLineItem) { ?>
						<tr>
							<td><?=$WOChemicalLineItem->ChemicalLineItemNo?></td>
							<td><a href="#"><?=$WOChemicalLineItem->ChemicalName?></a></td>
							<td><?=$WOChemicalLineItem->Measure?></td>
							<td><?=$WOChemicalLineItem->UnitOfMeasure?></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15"  data-toggle="modal" data-target="#create-chemical-line-items-modal" ><i class="fa fa-plus"></i> Add Chemical</a>
						<button type="button" class="btn btn-primary btn-outline mb-10 mt-15 edit-line-items" data-toggle="modal" data-target="#create-chemical-line-items-modal"> Edit Lines</button>
				</div>
				<?php if(count($WOChemicalLineItems) >= 10){?>
					<div class="pull-right">
						<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/WorkOrderLineItems" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
					</div>
				<?php } ?>
				</div>
			</div> 

			<div class="col-md-12">
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<h5 class="txt-dark mt-20"><small>Events</small> </h5>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Events</button>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="table-wrap">
				<div class="table-responsive">
				<table id="ContactsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Subject </th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Assigned To</th>
							<th>Name</th>
							<th>Created Date</th>
							<th>Created By</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Events as $key => $Event) { ?>
						<tr>
							<td><a href="#"><?=$Event->Subject?></a></td>
							<td><?=$Event->EventStartDate?></td>
							<td><?=$Event->EventEndDate?></td>
							<td><a href="#"><?=$Event->AssignedToName?></a></td>
							<td><a href="#"><?=$Event->ContactName?></a></td>
							<td><?=$Event->CreatedDate?></td>
							<td><a href="#"><?=$Event->CreatedBy?></a></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				</div>
				<?php if(count($Events) >= 10){?>
					<div class="pull-right">
						<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/Events" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
					</div>
				<?php } ?>
				</div>
			</div> 

			<div class="col-md-12">
			<div class="clearfix"></div>
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<h5 class="txt-dark mt-20"><small>Files</small> </h5>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New File</button>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="table-wrap">
			<div class="table-responsive">
			<table id="LocationsTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>File Name </th>
						<th>Content Type</th>
						<th>Subject</th>
						<th>Created Date</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Files as $key => $File) { ?>
						<tr>
							<td><?=$File->FileName?></td>
							<td><?=$File->ContentType?></td>
							<td><a href="#"><?=$File->Subject?></a></td>
							<td><?=$File->CreatedDate?></td>
							<td><a href="#"><?=$File->CreatedBy?></a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php if(count($Files) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/Files" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
			<?php } ?>
			</div>
			</div>

			<div class="col-md-12">
			<div class="clearfix"></div>
			
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<h5 class="txt-dark mt-20"><small>Tasks</small> </h5>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Task</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="table-wrap">
			<div class="table-responsive">
			<table id="WorkOrdersTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>Subject </th>
						<th>Name </th>
						<th>Type </th>
						<th>Assigned To </th>
						<th>Date</th>
						<th>Status</th>
						<th>Priority</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Tasks as $key => $Task) { ?>
						<tr>
							<td><a href="#"><?=$Task->Subject?></a></td>
							<td><a href="#"><?=$Task->ContactName?></a></td>
							<td><?=$Task->TaskType?></td>
							<td><a href="#"><?=$Task->AssignedToName?></a></td>
							<td><?=$Task->DueDate?></td>
							<td><?=$Task->TaskStatus?></td>
							<td><?=$Task->Priority?></td>
						
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php if(count($Tasks) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/Tasks" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
			<?php } ?>
			</div>
			</div>

			</div>
		</div>
	</div>
</div>