<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class AcActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Account/AcActionsModel');
    }
  
    function CreateAccount() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'PreferredTechnician' => $PreferredTechnician,
            'LastActivityDate' => $LastActivityDate,
            'LastServiceDate' => $LastServiceDate,
            'PrimaryContact' => $PrimaryContact,
            'IsActive' => $IsActive
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $AccountID = $this->AcActionsModel->CreateAccount($UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician,$LastActivityDate, $LastServiceDate, $PrimaryContact, $IsActive); 

        return API_Controller::successResponse(array("AccountID" => "$AccountID"),1, "Account has been created successfully.", "True");
    }

    function RecentAccounts() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = API_Controller::removeNull($this->AcActionsModel->RecentAccounts($UserID));
 
        $data['data'] = $result;//$this->AcActionsModel->RecentMyAccounts($UserID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }

    /*function RecentMyAccounts() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = API_Controller::removeNull($this->AcActionsModel->RecentMyAccounts($UserID));
 
        $data['data'] = $result;//$this->AcActionsModel->RecentMyAccounts($UserID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }
 
    function RecentAllAccounts() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $result = API_Controller::removeNull($this->AcActionsModel->RecentAllAccounts($UserID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }  */ 

    function AccountDetails() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->AcActionsModel->AccountDetails($UserID, $AccountID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account details fetch successfully.', 'True');
    }  

    function AccountRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedList($UserID, $AccountID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account related list fetch successfully.', 'True');
    }  

    function AccountRelatedContact() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedContact($UserID, $AccountID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account related contact fetch successfully.', 'True');
    }  

    function AccountRelatedLocation() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedLocation($UserID, $AccountID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account related location fetch successfully.', 'True');
    }  

    function AccountRelatedWorkOrder() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedWorkOrder($UserID, $AccountID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Account related work order fetch successfully.', 'True');
    }

    function AccountRelatedEstimate() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedEstimate($UserID, $AccountID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Account related estimate fetch successfully.', 'True');
    }

    function AccountRelatedInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedInvoice($UserID, $AccountID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Account related invoice fetch successfully.', 'True');
    }

    function AccountRelatedTask() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedTask($UserID, $AccountID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Account related task fetch successfully.', 'True');
    }

    function AccountRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedEvent($UserID, $AccountID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Account related event fetch successfully.', 'True');
    }

    function AccountRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedFile($UserID, $AccountID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account related file fetch successfully.', 'True');
    }
 
    function AccountRelatedChemical() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedChemical($UserID, $AccountID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account related chemical fetch successfully.', 'True');
    }

    function AccountRelatedProduct() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedProduct($UserID, $AccountID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account related product fetch successfully.', 'True');
    }

    function EditAccount() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

         API_Controller::requiredValidation([
            'AccountID' => $AccountID,
            'UserID' => $UserID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'PreferredTechnician' => $PreferredTechnician,
            'LastActivityDate' => $LastActivityDate,
            'LastServiceDate' => $LastServiceDate,
            'PrimaryContact' => $PrimaryContact,
            'IsActive' => $IsActive
        ]);

        API_Controller::checkUserAuthentication($UserID, $AccountID); 

        $result = $this->AcActionsModel->EditAccount($AccountID, $UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician,$LastActivityDate, $LastServiceDate, $PrimaryContact, $IsActive);  

        return API_Controller::responseMessage(1, "Account details has been updated successfully.", "True");
    }  

   /* function AccountFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0]
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountFilter($UserID, $FilterFields, $FilterConditions, $FilterValues);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account filter has been saved successfully.', 'True');
    } 
 
    function GetAccountFilter() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->AcActionsModel->GetAccountFilter($UserID);  

        API_Controller::successResponse($data, 1, 'Account filter fetch successfully.', 'True');
    } */
}
