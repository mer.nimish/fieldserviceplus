<?php

require_once(APPPATH.'controllers/API/API_Controller.php');
 
header('Content-type: application/json; charset=utf-8');


class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/User/AuthModel'); 
    }
 
    function SignUp() { 
     
        API_Controller::varifyMethod("POST");
  
        extract($_POST);

        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];

        API_Controller::requiredValidation([
            'key' => $key,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Email' => $Email,
            'Password' => $Password,
            'CompanyName' => $CompanyName,
            'PhoneNo' => $PhoneNo,
            'City' => $City,
            'DeviceUDID' => $DeviceUDID,
            'DeviceType' => $DeviceType
        ]); 
  
        $generateToken = API_Controller::generateToken();

        $result = $this->AuthModel->SignUp($key, $FirstName, $LastName, $CompanyName, $PhoneNo, $City, $Email, $Password, $DeviceUDID, $DeviceType, $generateToken); 

        if (is_array($result)){

            $result = API_Controller::removeNull($result);

            $result['vProfilePic'] = !empty($result['vProfilePic'])?IMAGE_URL.$result['vProfilePic']:"";
            $res_data = $result;
            $res_data['token'] = $generateToken;
            $data['data'] = $res_data;

            API_Controller::successResponse($data, 1, 'Registration has been completed successfully.', 'True');
        } 
        else if ($result == 2)
            return API_Controller::responseMessage(0, "Something went wrong while generating key, please try again.", "False");
        else if ($result == 3)
            return API_Controller::responseMessage(0, "This email has already been registered. Sign into your existing account, or register with a different email", "False");
    }
    
    function Login(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];

        API_Controller::requiredValidation([
            'Email' => $Email,
            'Password' => $Password,
            'DeviceUDID' => $DeviceUDID,
            'DeviceType' => $DeviceType,
            'key' => $key
        ]);

        $generateToken = API_Controller::generateToken();

        $result = $this->AuthModel->Login($key, $Email, $Password, $generateToken, $DeviceUDID, $DeviceType);

        if(is_array($result)){

            $result = API_Controller::removeNull($result);
            $result['token'] = $generateToken;
            $data['data'] = $result;

            API_Controller::successResponse($data, 1, 'Login successfully.', 'True');
        }
        else if ($result == 2)
            return API_Controller::responseMessage(0, "Something went wrong while generating key, please try again.", "False");
        else if ($result == 3)
            return API_Controller::responseMessage(0, "Wrong email or password", "False");
    }

    function ForgotPassword(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];

        API_Controller::requiredValidation([
            'Email' => $Email,
            'key' => $key
        ]);
 
        $generateToken = API_Controller::generateToken();
 
        $result = $this->AuthModel->ForgotPassword($key, $Email, $generateToken);

        if($result == 1){
            return API_Controller::responseMessage(1, 'A new password has been sent to your e-mail address.', 'True');
        }
        else if ($result == 2)
            return API_Controller::responseMessage(0, "Email not found.", "False");
    }

    function changePassword(){
        API_Controller::varifyMethod("POST");

        $getData = API_Controller::getPostData();

        extract($getData);

        API_Controller::requiredValidation([
            'iUserID' => $iUserID,
            'vPassword' => $vPassword,
            'vNewPassword' => $vNewPassword,
            'vConfirmPassword' => $vConfirmPassword
        ]);

        API_Controller::checkUserAuthentication($iUserID);

        if ($vNewPassword != $vConfirmPassword)
            return API_Controller::responseMessage(0, "Password does not match the confirm password.", "False");

        $result = $this->user_model->changePassword($iUserID, $vPassword, $vNewPassword);

       if (!is_array($result) && $result == 2)
            return API_Controller::responseMessage(0, "Wrong old password", "False");
        else {
            $res_data['iUserID'] = $result['iUserID'];
            $res_data['vEmail'] = $result['vEmail'];
            $data['data'] = $res_data;

            API_Controller::successResponse($data, 1, 'Password has been changed successfully.', 'True');
        }
    }

    function userLogout(){ 
        API_Controller::varifyMethod("POST");

        $getData = API_Controller::getPostData();

        extract($getData);

        API_Controller::checkUserAuthentication($iUserID);

        API_Controller::requiredValidation([
            'iUserID' => $iUserID
        ]);

        $result = $this->user_model->userLogout($iUserID);

        return API_Controller::responseMessage(1, 'Logout successfully.', 'True');
    }

}
