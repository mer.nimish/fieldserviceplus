<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class WoActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/WorkOrder/WoActionsModel');
    }
  
    function CreateWorkOrder() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'ParentWorkOrder' => $ParentWorkOrder,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'Street' => $Street,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'Discount' => $Discount,
            'GrandTotal' => $GrandTotal,
            'LineItemCount' => $LineItemCount,
            'SubTotal' => $SubTotal,
            'Tax' => $Tax,
            'TotalPrice' => $TotalPrice,
            'Signature' => $Signature,
            'PopUpReminder' => $PopUpReminder,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'PrimaryContact' => $PrimaryContact,
            'StartDate' => $StartDate,
            'EndDate' => $EndDate,
            'WOStartTime' => $WOStartTime,
            'WOEndTime' => $WOEndTime
        ]);

        if(isset($IsRecurring) && $IsRecurring == 1){
 
            $RepeatOn = !empty($_POST['RepeatOn'])?$_POST['RepeatOn']:NULL;
            $EndsOnDate = !empty($_POST['EndsOnDate'])?date("Y-m-d",strtotime($_POST['EndsOnDate'])):NULL;
            $EndsAfterOccurrences = !empty($_POST['EndsAfterOccurrences'])?$_POST['EndsAfterOccurrences']:NULL;
            API_Controller::requiredValidation([
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime
            ]);
        } else {
            $IsRecurring = 0;
            $RepeatEvery = NULL;
            $IntervalEvery = NULL;
            $Ends = NULL;
            $RepeatOn = NULL;
            $EndsOnDate = NULL;
            $EndsAfterOccurrences = NULL;
            $StartTime = NULL;
            $EndTime = NULL;  
        }
   
        API_Controller::checkUserAuthentication($UserID); 

        $WorkOrderID = $this->WoActionsModel->CreateWorkOrder($UserID, $ParentWorkOrder, $AssignedTo, $Account, $Subject, $Description, $Address, $Street, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $Discount, $GrandTotal, $LineItemCount, $SubTotal, $Tax, $TotalPrice, $Signature, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $WOCategory, $StartDate, $EndDate, $WOStartTime, $WOEndTime, $PrimaryContact, $IsRecurring, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences); 

        return API_Controller::successResponse(array("WorkOrderID" => "$WorkOrderID"), 1, "Work order has been created successfully.", "True");
    }

    function RecentMyWorkOrders() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->WoActionsModel->RecentMyWorkOrders($UserID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }
 
    function RecentAllWorkOrders() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->WoActionsModel->RecentAllWorkOrders($UserID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }  

    function RecentWorkOrders() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->WoActionsModel->RecentWorkOrders($UserID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function WorkOrderDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->WoActionsModel->WorkOrderDetails($UserID, $WorkOrderID));

        $data['data'] = $result                                                                                                                                                                                                                                                                                                         ;  

        API_Controller::successResponse($data, 1, 'WorkOrder details fetch successfully.', 'True');
    }  

    function WorkOrderRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedList($UserID, $WorkOrderID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'WorkOrder related list fetch successfully.', 'True');
    }  

    function WorkOrderRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedEvent($UserID, $WorkOrderID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'WorkOrder related event fetch successfully.', 'True');
    }  

    function WorkOrderRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedFile($UserID, $WorkOrderID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'WorkOrder related file fetch successfully.', 'True');
    }  

    function WorkOrderRelatedTask() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedTask($UserID, $WorkOrderID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'WorkOrder related task fetch successfully.', 'True');
    }

    function EditWorkOrder() { 
      
        API_Controller::varifyMethod("POST");

        $WORecurrenceID = !empty($_POST['WORecurrenceID'])?$_POST['WORecurrenceID']:NULL;
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'WorkOrderID' => $WorkOrderID,
            'ParentWorkOrder' => $ParentWorkOrder,
            'UserID' => $UserID,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'Street' => $Street,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'PopUpReminder' => $PopUpReminder,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'PrimaryContact' => $PrimaryContact,
            'StartDate' => $StartDate,
            'EndDate' => $EndDate,
            'WOStartTime' => $WOStartTime,
            'WOEndTime' => $WOEndTime,
            'WOCategory' => $WOCategory,
            'SubTotal' => $SubTotal,
            'Tax' => $Tax,
            'TotalPrice' => $TotalPrice,
            'GrandTotal' => $GrandTotal,
            'Signature' => $Signature,
            'Discount' => $Discount,
            'LineItemCount' => $LineItemCount,
        ]);

        if(isset($IsRecurring) && $IsRecurring == 1){

            $RepeatOn = !empty($_POST['RepeatOn'])?$_POST['RepeatOn']:NULL;
            $EndsOnDate = !empty($_POST['EndsOnDate'])?date("Y-m-d",strtotime($_POST['EndsOnDate'])):NULL;
            $EndsAfterOccurrences = !empty($_POST['EndsAfterOccurrences'])?$_POST['EndsAfterOccurrences']:NULL; 
            API_Controller::requiredValidation([
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime
            ]);
        } else {
            $IsRecurring = 0;
            $RepeatEvery = NULL;
            $IntervalEvery = NULL;
            $Ends = NULL;
            $RepeatOn = NULL;
            $EndsOnDate = NULL;
            $EndsAfterOccurrences = NULL;
            $StartTime = NULL;
            $EndTime = NULL;  
        }
   
        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->WoActionsModel->EditWorkOrder($WorkOrderID, $ParentWorkOrder, $UserID, $Account, $AssignedTo, $Subject, $Description, $Address, $Street, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $PrimaryContact, $StartDate, $EndDate, $WOStartTime, $WOEndTime, $WOCategory, $SubTotal, $Tax, $TotalPrice, $GrandTotal, $Signature, $Discount, $LineItemCount, $IsRecurring, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $WORecurrenceID); 

        return API_Controller::responseMessage(1, "Work order has been updated successfully.", "True");
    }  
}
