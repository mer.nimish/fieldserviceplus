<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Account/ViewModel');
    }
   
    function index($AccountViewID = 0) {

        if(isset($AccountViewID) && $AccountViewID != 0)
            $data['SelectedAccount'] = $AccountViewID;
        else 
            $data['SelectedAccount'] = isset($_POST['SelectedAccount'])?$_POST['SelectedAccount']:"";

        if($data['SelectedAccount'] == 'MyAccounts' || $data['SelectedAccount'] == 'AllAccounts' || $data['SelectedAccount'] == 'NewAccountsThisWeek' || $data['SelectedAccount'] == ""){
            $this->load->view('Account/AccountView', $data);
        }
        else 
        {
            $data['AccountCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedAccount']);
            $data['AccountData'] = $this->ViewModel->GetAccountViewData($data['SelectedAccount']);
           
            $this->load->view('Account/AccountCustomView', $data);
        }
    }

    function ViewAccounts() { 
        echo $this->ViewModel->ViewAccounts();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveAccountFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredAccounts'] = $result; 
            $this->load->view('Account/AccountViewResult', $data);

         } else { 
            $this->load->view('Account/AccountViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['AccountViewFields'] = $this->ViewModel->AccountViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $AccountViewID = $this->ViewModel->CreateNewView(); 

            if($AccountViewID){

                $this->session->set_flashdata('SuccessMsg', 'Account new view created successfully.');
                redirect('Account/View/Index/'.$AccountViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Account/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Account/CreateNewView', $data);
        }
    }

    function EditCustomView($AccountViewID) { 

        $data = $this->ViewModel->GetCustomView($AccountViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($AccountViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Account view updated successfully.');
                redirect('Account/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Account/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Account/EditCustomView', $data);
        }
    }

    function DeleteCustomView($AccountViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($AccountViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Account view deleted successfully.');
            redirect('Account/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);
        }
    }

    function CopyCustomView($AccountViewID, $AccountViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($AccountViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Account view copy successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);
        }
    }

    function RenameCustomView($AccountViewID){

        $result =  $this->ViewModel->RenameCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view rename successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }

    function EditSharingCustomView($AccountViewID){

        $result =  $this->ViewModel->EditSharingCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view sharing updated successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }
  
    function EditFiltersCustomView($AccountViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view filters updated successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }

    function EditDisplayedColumnsCustomView($AccountViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view displayed columns updated successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }
}
