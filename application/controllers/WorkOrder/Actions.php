<?php

class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('WorkOrder/ActionsModel');
    }
   
    function index() { 
        $this->load->view('WorkOrder/WorkOrderHome');
    }

    function RecentWorkOrders() { 
        echo $this->ActionsModel->RecentWorkOrders();
    }
  
    function CreateWorkOrder($WorkOrderID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateWorkOrder'])){

            $this->form_validation->set_rules('AssignedTo', 'owner', 'required');
            $this->form_validation->set_rules('ParentWorkOrder', 'parent work order', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Description', 'description', 'required');
            $this->form_validation->set_rules('Address', 'address', 'required');  
            //$this->form_validation->set_rules('Street', 'street', 'required');
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('WorkOrderType', 'work order type', 'required');
            $this->form_validation->set_rules('WOStatus', 'status', 'required');
            $this->form_validation->set_rules('WOPriority', 'priority', 'required');
            $this->form_validation->set_rules('WOCategory', 'category', 'required');
            $this->form_validation->set_rules('StartDateTime', 'start datetime', 'required');
            $this->form_validation->set_rules('EndDateTime', 'end datetime', 'required');
            $this->form_validation->set_rules('PrimaryContact', 'primary contact', 'required');
            $this->form_validation->set_rules('SubTotal', 'sub total', 'required');
            $this->form_validation->set_rules('Tax', 'tax', 'required');
            $this->form_validation->set_rules('TotalPrice', 'total price', 'required');
            $this->form_validation->set_rules('GrandTotal', 'grand total', 'required');
            $this->form_validation->set_rules('Signature', 'signature', 'required');
            $this->form_validation->set_rules('Discount', 'discount', 'required');
            $this->form_validation->set_rules('LineItemCount', 'line item count', 'required');
            $this->form_validation->set_rules('PrimaryContact', 'primary contact', 'required');
            //$this->form_validation->set_rules('PopUpReminder', 'popup reminder', 'required');

            if(isset($_POST['IsRecurring']) && $_POST['IsRecurring']==1){
                $this->form_validation->set_rules('WORecurrenceID', 'recurrence', 'required');
            } 
            
            if ($this->form_validation->run() == TRUE)
            {   
                $result = $this->ActionsModel->CreateWorkOrder();    
                if($result){ 

                    $this->session->set_flashdata('SuccessMsg', 'Work order created successfully.');
                    redirect('WorkOrder/Actions');

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('WorkOrder/Actions/CreateWorkOrder');
                }
            } else {
                $this->load->view('WorkOrder/CreateWorkOrder');
            }

        } else { 

            $data =  $this->ActionsModel->getWorkOrderData($WorkOrderID);
            $this->load->view('WorkOrder/CreateWorkOrder', $data);
        }
    }

    function EditWorkOrder($WorkOrderID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getWorkOrderData($WorkOrderID); 

        if(isset($_POST['EditWorkOrder'])){

            $this->form_validation->set_rules('AssignedTo', 'owner', 'required');
            $this->form_validation->set_rules('ParentWorkOrder', 'parent work order', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Subject', 'Subject', 'required');
            $this->form_validation->set_rules('Description', 'description', 'required');
            $this->form_validation->set_rules('Address', 'address', 'required');  
            //$this->form_validation->set_rules('Street', 'street', 'required');
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('SubTotal', 'sub total', 'required');
            $this->form_validation->set_rules('Tax', 'tax', 'required');
            $this->form_validation->set_rules('TotalPrice', 'total price', 'required');
            $this->form_validation->set_rules('GrandTotal', 'grand total', 'required');
            $this->form_validation->set_rules('Signature', 'signature', 'required');
            $this->form_validation->set_rules('Discount', 'discount', 'required');
            $this->form_validation->set_rules('LineItemCount', 'line item count', 'required');
            $this->form_validation->set_rules('WorkOrderType', 'work order type', 'required');
            $this->form_validation->set_rules('WOStatus', 'status', 'required');
            $this->form_validation->set_rules('WOPriority', 'priority', 'required');
            $this->form_validation->set_rules('WOCategory', 'category', 'required');
           /* $this->form_validation->set_rules('StartDateTime', 'start datetime', 'required');
            $this->form_validation->set_rules('EndDateTime', 'end datetime', 'required');*/
            $this->form_validation->set_rules('PrimaryContact', 'primary contact', 'required');
            //$this->form_validation->set_rules('PopUpReminder', 'popup reminder', 'required');
            
            if(isset($_POST['IsRecurring']) && $_POST['IsRecurring']==1){
                $this->form_validation->set_rules('WORecurrenceID', 'recurrence', 'required');
            } 

            if ($this->form_validation->run() == TRUE)
            {  
               
                
                $result = $this->ActionsModel->EditWorkOrder($WorkOrderID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'WorkOrder updated successfully.');
                    redirect('WorkOrder/Actions');

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('WorkOrder/Actions/EditWorkOrder/'.$WorkOrderID); 
                }
            } else { 
                $this->load->view('WorkOrder/EditWorkOrder', $data); 
            }

        } else { 
          
            $this->load->view('WorkOrder/EditWorkOrder', $data);
        }
    }

    function RelatedObject($WorkOrderID, $RelatedObjName) {
 
        $RelatedObjData =  $this->ActionsModel->getWorkOrderData($WorkOrderID, $RelatedObjName); 
        $WorkOrderData =  $this->ActionsModel->getWorkOrderData($WorkOrderID);
        $data['RelatedObjData'] = $RelatedObjData;
        $data['WorkOrderData'] = $WorkOrderData['WorkOrderData'];
        $data['RelatedObjName'] = $RelatedObjName;
        $this->load->view('WorkOrder/WorkOrderRelated'.$RelatedObjName, $data);
    }

    function CreateNote($WorkOrderID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($WorkOrderID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('WorkOrder/Actions/WorkOrderDetails/'.$WorkOrderID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('WorkOrder/Actions/WorkOrderDetails/'.$WorkOrderID);
                }
            }
        }
    }

    function WorkOrderDetails($WorkOrderID) {

        $data =  $this->ActionsModel->getWorkOrderData($WorkOrderID); 
        $this->load->view('WorkOrder/WorkOrderDetails', $data);
    }

    function WOLineItem($WorkOrderID)
    {
        $result =  $this->ActionsModel->WOLineItem($WorkOrderID); 
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Line item saved successfully.');
            redirect('WorkOrder/Actions/WorkOrderDetails/'.$WorkOrderID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/Actions/WorkOrderDetails/'.$WorkOrderID);
        }
    }

    function GetProductDetails($ProductID)
    {
        echo $this->ActionsModel->GetProductDetails($ProductID); 
    }

    function GetPrimaryContactOptions()
    {
        echo $this->ActionsModel->GetPrimaryContactOptions(); 
    }

    function SaveScheduling()
    {
        echo $this->ActionsModel->SaveScheduling(); 
    }

    function GetScheduling($EventID)
    {
        echo $this->ActionsModel->GetScheduling($EventID); 
    }

    function UpdateScheduling($EventID)
    {
        echo $this->ActionsModel->UpdateScheduling($EventID); 
    }

    function DeleteScheduling($EventID)
    {
        echo $this->ActionsModel->DeleteScheduling($EventID);
    }

    function EditSchAssignedTo()
    { 
        echo $this->ActionsModel->EditSchAssignedTo();
    }

    function UpdateAssignedTo()
    {
         echo $this->ActionsModel->UpdateAssignedTo();
    }

    function saveRecurrenceSetting()
    {
        echo $this->ActionsModel->saveRecurrenceSetting();
    }

    function removeRecurrenceSetting()
    {
        echo $this->ActionsModel->removeRecurrenceSetting();
    }

    function getState(){
        echo MY_Model::getState($_POST['CountryID']);
    }

    function getCity(){
        echo MY_Model::getCity($_POST['StateID']);
    } 

    function DeleteWorkOrder($WorkOrderID){
        $result =  $this->ActionsModel->DeleteWorkOrder($WorkOrderID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Work Order deleted successfully.');
            redirect('WorkOrder/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/Actions/WorkOrderDetails/'.$WorkOrderID); 
        }
    }
}
