<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('WorkOrder/ViewModel');
    }
   
    function index($WorkOrderViewID = 0) {

        if(isset($WorkOrderViewID) && $WorkOrderViewID != 0)
            $data['SelectedWorkOrder'] = $WorkOrderViewID;
        else 
            $data['SelectedWorkOrder'] = isset($_POST['SelectedWorkOrder'])?$_POST['SelectedWorkOrder']:"";

        if($data['SelectedWorkOrder'] == 'MyOpenWorkOrdersToday' 
            || $data['SelectedWorkOrder'] == 'MyOpenWorkOrdersThisWeek' 
            || $data['SelectedWorkOrder'] == 'AllOpenWorkOrdersToday' 
            || $data['SelectedWorkOrder'] == 'AllWorkOrdersToday' 
            || $data['SelectedWorkOrder'] == 'AllOpenWorkOrdersThisWeek' 
            || $data['SelectedWorkOrder'] == 'WorkOrdersCreatedToday' 
            || $data['SelectedWorkOrder'] == 'WorkOrdersCreatedThisWeek' 
            || $data['SelectedWorkOrder'] == 'UnscheduledWorkOrders' 
            || $data['SelectedWorkOrder'] == 'UnassignedWorkOrders' 
            || $data['SelectedWorkOrder'] == 'MyOpenWorkOrdersPastDue' 
            || $data['SelectedWorkOrder'] == 'AllOpenWorkOrdersPastDue' 
            || $data['SelectedWorkOrder'] == ""){
           
            $this->load->view('WorkOrder/WorkOrderView', $data);
        }
        else 
        {
            $data['WorkOrderCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedWorkOrder']);
            $data['WorkOrderData'] = $this->ViewModel->GetWorkOrderViewData($data['SelectedWorkOrder']);
           
            $this->load->view('WorkOrder/WorkOrderCustomView', $data);
        }
    }

    function ViewWorkOrders() { 
        echo $this->ViewModel->ViewWorkOrders();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveWorkOrderFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredWorkOrders'] = $result; 
            $this->load->view('WorkOrder/WorkOrderViewResult', $data);

         } else { 
            $this->load->view('WorkOrder/WorkOrderViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['WorkOrderViewFields'] = $this->ViewModel->WorkOrderViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $WorkOrderViewID = $this->ViewModel->CreateNewView(); 

            if($WorkOrderViewID){

                $this->session->set_flashdata('SuccessMsg', 'WorkOrder new view created successfully.');
                redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('WorkOrder/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('WorkOrder/CreateNewView', $data);
        }
    }

    function EditCustomView($WorkOrderViewID) { 

        $data = $this->ViewModel->GetCustomView($WorkOrderViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($WorkOrderViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'WorkOrder view updated successfully.');
                redirect('WorkOrder/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('WorkOrder/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('WorkOrder/EditCustomView', $data);
        }
    }

    function DeleteCustomView($WorkOrderViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($WorkOrderViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view deleted successfully.');
            redirect('WorkOrder/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);
        }
    }

    function CopyCustomView($WorkOrderViewID, $WorkOrderViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($WorkOrderViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view copy successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);
        }
    }

    function RenameCustomView($WorkOrderViewID){

        $result =  $this->ViewModel->RenameCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view rename successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }

    function EditSharingCustomView($WorkOrderViewID){

        $result =  $this->ViewModel->EditSharingCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view sharing updated successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }
  
    function EditFiltersCustomView($WorkOrderViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view filters updated successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }

    function EditDisplayedColumnsCustomView($WorkOrderViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view displayed columns updated successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }
}
