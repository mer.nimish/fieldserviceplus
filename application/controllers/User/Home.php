<?php

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User/AuthModel');
    }

    function index() { 

        $this->load->view('User/DashboardView');
    }
}
