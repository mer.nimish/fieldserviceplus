<?php
class MY_Model extends CI_Model
{  
    function __construct() {
        parent::__construct();
    }

    static function insertCreatedModifiedByAndDt($UserID, $TableName, $Where)
    { 
        $CI=&get_instance();
        $data = array(
            'CreatedDate' => date('Y-m-d H:i:s'),
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'CreatedBy' => $UserID,
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }
 
    static function updateCreatedModifiedByAndDt($UserID, $TableName, $Where)
    {
        $CI=&get_instance();
        $data = array(
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }

    static function getCountry()
    {
        $CI=&get_instance();
        $query = $CI->db->select('CountryID, CountryName')->get('Country');
        return $query->result(); 
    }

    static function getState($CountryID)
    {
        $CI=&get_instance();
        $statestr = "";
        $query = $CI->db->select('StateID, StateName')->get_where('State',array('Country'=>$CountryID));
        $results = $query->result();

        foreach ($results as $key => $value) {
           $statestr .= '<option value="'.$value->StateID.'">'.$value->StateName.'</option>';
        }
        return $statestr; 
    }

    static function getCity($StateID)
    {
        $CI=&get_instance();
        $citystr = "";
        $query = $CI->db->select('CityID, CityName')->get_where('City',array('State'=>$StateID));
        $results = $query->result();

        foreach ($results as $key => $value) {
           $citystr .= '<option value="'.$value->CityID.'">'.$value->CityName.'</option>';
        }
        return $citystr; 
    }

    static function getTax()
    {
        $CI=&get_instance();
        $query = $CI->db->select('Tax')->get('Settings');
        return $query->row()->Tax; 
    }
}