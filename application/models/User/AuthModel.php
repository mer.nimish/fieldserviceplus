<?php

class AuthModel extends MY_Model {
 
    function __construct() {
        parent::__construct();
    }

    function checkUserAuthentication($UserID, $token) {
        $data = array(
            'UserID' => $UserID,
            'Token' => $token
        );

        $query = $this->db->get_where('User', $data);
        return $query->num_rows();
    }
 
    function SignUp($FirstName, $LastName, $CompanyName, $PhoneNo, $City, $Email, $Password) {
        $data = array(
            'Email' => $Email
        );
        $query = $this->db->get_where('User', $data);
 
        if ($query->num_rows() > 0) {
            return 0;
        } else {

            $data = array(
                'FirstName' => $FirstName,
                'LastName' => $LastName,
                'CompanyName' => $CompanyName,
                'PhoneNo' => $PhoneNo,
                'City' => $City,
                'Email' => $Email,
                'Password' => md5($Password)
            );
            $result = $this->db->insert('User', $data);

            $UserID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'User', array('UserID' => $UserID));
         
            return 1;
        }
    }
 
    function Login($Email, $Password){

        $where = array(
            'Email' => $Email,
            'Password' => md5($Password)
        );
        $query = $this->db->get_where('User', $where);
    
        if ($query->num_rows() > 0) {

            $userdata = $query->row_array();

            $this->session->set_userdata($userdata);
 
            return 1; 

        } else {
            return 0; 
        }
    }  

    function ForgotPassword($Email){
 
        $where = array(
            'Email' => $Email
        );
        $query = $this->db->get_where('User', $where);
    
        if ($query->num_rows() > 0) {

            $userdata = $query->row_array();

            $Password = MY_Controller::genNumRandCode(6);

            $message = "Hello, <br /><br /> Your new password is $Password <br /><br /> Thank You";  
 
            MY_Controller::sendMail($Email, 'Forgot Password', $message);

            $data = array('Password' => md5($Password));

            $result = $this->db->update('User', $data, array('UserID' => $userdata['UserID']));
 
            return 1; 

        } else {
            return 0; 
        }
    }  
    
}

?>
