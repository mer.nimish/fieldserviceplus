<?php

class API_Model extends CI_Model
{   
    function __construct() {
        parent::__construct();
    }

    static function checkQuery($Query)
    {
        $CI=&get_instance();
        try {

            if( !$Query )
            {
                $error = $CI->db->error();
                throw new Exception(API_Controller::responseMessage($error['code'], $error['message'], "False"));
            }
        }
        catch (Exception $e){ exit;}  
        
    }

    static function insertCreatedModifiedByAndDt($UserID, $TableName, $Where)
    { 
        $CI=&get_instance();
        $data = array(
            'CreatedDate' => date('Y-m-d H:i:s'),
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'CreatedBy' => $UserID,
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }
 
    static function updateCreatedModifiedByAndDt($UserID, $TableName, $Where)
    {
        $CI=&get_instance();
        $data = array(
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }

    static function getCountry()
    {
        $CI=&get_instance();
        $query = $CI->db->select('CountryID, CountryName')->get('Country');
        return $query->result(); 
    }  
}