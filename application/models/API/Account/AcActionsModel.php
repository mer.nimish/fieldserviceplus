<?php
require_once(APPPATH.'models/API/API_Model.php');

class AcActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateAccount($UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician,$LastActivityDate, $LastServiceDate, $PrimaryContact, $IsActive) {
                 
        $data = array(
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'PreferredTechnician' => $PreferredTechnician,
            'BillingAddress' => $BillingAddress,
            'BillingState' => $BillingState,
            'BillingCity' => $BillingCity,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingState' => $ShippingState,
            'ShippingCity' => $ShippingCity,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'LastActivityDate' => date("Y-m-d",strtotime($LastActivityDate)),
            'LastServiceDate' => date("Y-m-d",strtotime($LastServiceDate)),
            'PrimaryContact' => $PrimaryContact,
            'IsActive' => $IsActive
        ); 
        $query = $this->db->insert('Account', $data);  

        API_Model::checkQuery($query);   

        $AccountID = $this->db->insert_id();

        $query = $this->db->update('Account', array('AccountNo'=>'AC'.'-'.sprintf("%05d", $AccountID)), array('AccountID' => $AccountID)); 

        API_Model::checkQuery($query);    

        API_Model::insertCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));
    
        return $AccountID;
    }

    function RecentAccounts($UserID)
    { 
        $query = $this->db->select('a.AccountID, a.AccountName, a.PhoneNo, at.AccountType, a.BillingCity, a.BillingState, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedTo')
        ->from('Account a')
        ->join('User ast','ast.UserID = a.AssignedTo', 'left') 
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left') 
        ->where(array('a.IsActive' => 1, 'a.IsDeleted' => 0))
        ->order_by("a.LastModifiedDate", "desc")
        ->get();   
 
        API_Model::checkQuery($query); 

        return $query->result();
    }
 
    /*function RecentMyAccounts($UserID)
    { 
        $query = $this->db->select('a.AccountID, a.AccountName, a.BillingCity as CityName, a.BillingState as StateName, at.AccountType, a.PhoneNo')
        ->from('Account a')
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left') 
        ->where(array('a.AssignedTo' => $UserID,'a.IsActive' => 1, 'a.IsDeleted' => 0))
        ->get();   
 
        API_Model::checkQuery($query); 

        return $query->result();
    }
 
    function RecentAllAccounts($UserID)
    { 
        $query = $this->db->select('a.AccountID, a.AccountName, a.BillingCity as CityName, a.BillingState as StateName, at.AccountType, a.PhoneNo')
        ->from('Account a')
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left') 
        ->where(array('a.IsActive' => 1, 'a.IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }*/

    function AccountDetails($UserID, $AccountID)
    { 
        $query = $this->db->select('a.*, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName, CONCAT(pmt.FirstName, " ", pmt.LastName) as PreferredTechnicianName, at.AccountType as AccountTypeName, DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'") as LastActivityDate, DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'") as LastServiceDate, DATE_FORMAT(a.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(a.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('Account a')
        ->join('User cb','cb.UserID = a.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left')
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left')
        ->join('User ast','ast.UserID = a.AssignedTo', 'left')
        ->join('User pmt','pmt.UserID = a.PreferredTechnician', 'left')
        ->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left')
        ->where(array('a.AccountID' => $AccountID,'a.IsActive' => 1, 'a.IsDeleted' => 0))
        ->get();

        API_Model::checkQuery($query); 

        $data = $query->row_array(); 
       
        return $data;
    }

    function AccountRelatedList($UserID, $AccountID)
    { 
        
        $query = $this->db->select('Count(*) as ContactCnt')
        ->get_where('Contact', array('Account' => $AccountID,'IsActive' => 1, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Contact']['title'] = 'Contacts ('.$result->ContactCnt.')';

        $query = $this->db->select('Count(*) as LocationCnt')
        ->get_where('Location', array('Account' => $AccountID,'IsActive' => 1, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Location']['title'] = 'Locations ('.$result->LocationCnt.')';

        $query = $this->db->select('Count(*) as WorkOrderCnt')
        ->get_where('WorkOrder', array('Account' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['WorkOrder']['title'] = 'Work Orders ('.$result->WorkOrderCnt.')';

        $query = $this->db->select('Count(*) as QuoteCnt')
        ->get_where('Quote', array('Account' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Estimate']['title'] = 'Estimates ('.$result->QuoteCnt.')';

        $query = $this->db->select('Count(*) as InvoiceCnt')
        ->get_where('Invoice', array('Account' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Invoice']['title'] = 'Invoices ('.$result->InvoiceCnt.')';

        $query = $this->db->select('Count(*) as FileCnt')
         ->get_where('File', array('RelatedTo' => 'Account', 'What' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('RelatedTo' => 'Account', 'What' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('RelatedTo' => 'Account', 'What' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';

        $query = $this->db->select('Count(*) as ChemicalCnt')
        ->get_where('Invoice', array('Account' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Chemical']['title'] = 'Chemicals ('.$result->ChemicalCnt.')';

        $query = $this->db->select('Count(*) as ProductCnt')
        ->get_where('Product', array('Account' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Product']['title'] = 'Installed Products ('.$result->ProductCnt.')';

        return $data;
    }

    function AccountRelatedContact($UserID, $AccountID)
    { 
        $query = $this->db->select('c.ContactID, c.FirstName, c.LastName, c.Title, c.PhoneNo, c.Email')
        ->from('Contact c') 
        ->join('User cb','cb.UserID = c.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = c.LastModifiedBy', 'left')
        ->where(array('c.Account' => $AccountID,'c.IsActive' => 1, 'c.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedLocation($UserID, $AccountID)
    { 
        $query = $this->db->select('l.LocationID, l.Name, l.LocationStreet, c.CityName')
        ->from('Location l') 
        ->join('City c','c.CityID = l.LocationCity', 'left')
        ->where(array('l.Account' => $AccountID,'l.IsActive' => 1, 'l.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedWorkOrder($UserID, $AccountID)
    { 
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status, woc.CategoryName, DATE_FORMAT(wo.StartDate, "'.RES_DATETIME.'") as StartDate') 
        ->from('WorkOrder wo') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = wo.WOCategory', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->where(array('wo.Account' => $AccountID, 'wo.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedEstimate($UserID, $AccountID)
    { 
        $query = $this->db->select('q.QuoteNumber, q.QuoteName, DATE_FORMAT(q.ExpirationDate, "'.RES_DATE.'") as ExpirationDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, q.Status, q.GrandTotal, DATE_FORMAT(q.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Quote q') 
        ->join('User o','o.UserID = q.Owner', 'left')
        ->where(array('q.Account' => $AccountID, 'q.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedInvoice($UserID, $AccountID)
    { 
        $query = $this->db->select('i.InvoiceNumber, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as DueDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, is.InvoiceStatus, i.SubTotal, i.TotalPrice, wo.Subject') 
        ->from('Invoice i') 
        ->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left')
        ->where(array('i.Account' => $AccountID, 'i.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedTask($UserID, $AccountID)
    { 
        $query = $this->db->select('t.Subject, CONCAT(c.FirstName, " ", c.LastName) as Name, tt.TaskType, DATE_FORMAT(t.ActivityDate, "'.RES_DATE.'") as Date, CONCAT(u.FirstName, " ", u.LastName) as AssignedTo, ts.TaskStatus, tp.Priority') 
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('Contact c','c.ContactID = t.Who', 'left')
        ->join('User u','u.UserID = t.AssignedTo', 'left')
        ->where(array('t.RelatedTo' => 'Account', 't.What' => $AccountID, 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedEvent($UserID, $AccountID)
    { 
        $query = $this->db->select('e.Subject, CONCAT(c.FirstName, " ", c.LastName) as Name, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, CONCAT(u.FirstName, " ", u.LastName) as AssignedTo, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Event e') 
        ->join('Contact c','c.ContactID = e.Who', 'left')
        ->join('User u','u.UserID = e.AssignedTo', 'left')
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->where(array('e.RelatedTo' => 'Account', 'e.What' => $AccountID, 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedFile($UserID, $AccountID)
    { 
        $query = $this->db->select('f.FileName, f.ContentType, f.Subject, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('File f') 
        ->join('User u','u.UserID = f.AssignedTo', 'left')
        ->join('User cb','cb.UserID = f.CreatedBy', 'left')
        ->where(array('f.RelatedTo' => 'Account', 'f.What' => $AccountID, 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedChemical($UserID, $AccountID)
    {  
        $query = $this->db->select('c.ChemicalNo, p.ProductName, p.Account, wo.Subject as WorkOrder, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(c.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Chemical c') 
        ->join('User cb','cb.UserID = c.CreatedBy', 'left')
        ->join('Product p','p.ProductID = c.Product', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = c.WorkOrder', 'left')
        ->where(array('c.Account' => $AccountID, 'c.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedProduct($UserID, $AccountID)
    {  
        $query = $this->db->select('p.ProductCode, p.ProductName, p.DefaultQuantity, p.ListPrice, p.DatePurchased, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(p.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Product p')  
        ->join('User cb','cb.UserID = p.CreatedBy', 'left')
        ->where(array('p.Account' => $AccountID, 'p.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EditAccount($AccountID, $UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician,$LastActivityDate, $LastServiceDate, $PrimaryContact, $IsActive) {
           
        $data = array(
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'PreferredTechnician' => $PreferredTechnician,
            'BillingAddress' => $BillingAddress,
            'BillingState' => $BillingState,
            'BillingCity' => $BillingCity,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingState' => $ShippingState,
            'ShippingCity' => $ShippingCity,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'LastActivityDate' => date("Y-m-d",strtotime($LastActivityDate)),
            'LastServiceDate' => date("Y-m-d",strtotime($LastServiceDate)),
            'PrimaryContact' => $PrimaryContact,
            'IsActive' => $IsActive
        );       
       
        $result = $this->db->update('Account', $data, array('AccountID' => $AccountID)); 

        API_Model::checkQuery($result); 

        API_Model::updateCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));
    
        return $result;
    }

    /*function AccountFilter($UserID, $FilterFields, $FilterConditions, $FilterValues)
    { 

        if(count($FilterValues) > 0){ 

            $res = $this->db->delete('AccountFilter', array('UserID' => $UserID));

            foreach ($FilterValues as $key => $value) {

                API_Model::checkQuery($res);
 
                    $data = array(
                        'UserID' => $UserID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('AccountFilter', $data);  

                    API_Model::checkQuery($query);
            }

            // Filter Account Data Start 
            $this->db->select('a.AccountID, a.PhoneNo, a.AccountName,  CONCAT(c.CityName,", ",st.StateName) as Address, at.AccountType');
            $this->db->from('Account a');
            $this->db->join('City c','c.CityID = a.BillingCity', 'left');
            $this->db->join('State st','st.StateID = c.State', 'left');
            $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
            $this->db->where(array('a.IsActive' => 1, 'a.IsDeleted' => 0));

            foreach ($FilterValues as $key => $value) {
                if($FilterConditions[$key] == 'Equals'){
                    $this->db->where($FilterFields[$key], $FilterValues[$key]);  
                } else if($FilterConditions[$key] == 'Contains'){
                    $this->db->like($FilterFields[$key], $FilterValues[$key]);   
                } else if($FilterConditions[$key] == 'StartsWith'){
                    $this->db->like($FilterFields[$key], $FilterValues[$key], 'after');   
                } else if($FilterConditions[$key] == 'DoesNotContain'){
                    $this->db->not_like($FilterFields[$key], $FilterValues[$key]);   
                } 
            } 
            $query = $this->db->get(); 
        
            API_Model::checkQuery($query); 

            return $query->result();
 
        }
    }
 
    function GetAccountFilter($UserID)
    { 
        $query = $this->db->get_where('AccountFilter', array('UserID' => $UserID)); 
        
        API_Model::checkQuery($query); 

        return $query->result();
    }*/

}

?>
