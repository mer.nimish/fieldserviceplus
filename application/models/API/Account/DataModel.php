<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetAccountType($UserID)
    { 
        $query = $this->db->select('AccountTypeID, AccountType')
        ->from('AccountType')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetAccounts($UserID)
    { 
        $query = $this->db->select('AccountID, AccountName')
        ->from('Account')
        ->where('IsActive', 1)
        ->where('IsDeleted', 0)
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
