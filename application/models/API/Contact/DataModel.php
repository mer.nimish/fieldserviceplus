<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetContacts($UserID)
    {  
        $query = $this->db->select('ContactID, CONCAT(FirstName," ",LastName) as FullName')
        ->from('Contact')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
