<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }

    function GetAllUsers()
    { 
        $query = $this->db->select('u.UserID, CONCAT(u.FirstName," ",u.LastName) as FullName')
        ->from('User u')
        ->where('u.IsActive', 1)
        ->where('u.IsDeleted', 0)
        ->get();  

        API_Model::checkQuery($query);  
          
        return $query->result_array();
    }

}

?>
