<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetWorkOrderTypes($UserID)
    { 
        $query = $this->db->select('WorkOrderTypeID, WorkOrderType')
        ->from('WorkOrderType')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetWorkOrderPriorities($UserID)
    { 
        $query = $this->db->select('WOPriorityID, Priority')
        ->from('WOPriority')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetWorkOrderStatus($UserID)
    { 
        $query = $this->db->select('WOStatusID, Status')
        ->from('WOStatus')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetWorkOrderCategories($UserID)
    { 
        $query = $this->db->select('WOCategoryID, CategoryName')
        ->from('WOCategory')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetParentWorkOrders($UserID)
    { 
        $query = $this->db->select('WorkOrderID, Subject')
        ->from('WorkOrder')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
