<?php
require_once(APPPATH.'models/API/API_Model.php');

class WoActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateWorkOrder($UserID, $ParentWorkOrder, $AssignedTo, $Account, $Subject, $Description, $Address, $Street, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $Discount, $GrandTotal, $LineItemCount, $SubTotal, $Tax, $TotalPrice, $Signature, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $WOCategory, $StartDate, $EndDate, $WOStartTime, $WOEndTime, $PrimaryContact, $IsRecurring, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences) {
        
        $WORecurrenceID = NULL;   
        if(isset($IsRecurring) && $IsRecurring == 1){ 
            $data = array(
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime,
                'RepeatOn' => $RepeatOn,
                'EndsOnDate' => $EndsOnDate,
                'EndsAfterOccurrences' => $EndsAfterOccurrences,
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            );   
            $query = $this->db->insert('WORecurrence', $data);

            API_Model::checkQuery($query); 

            $WORecurrenceID = $this->db->insert_id();
        } 
 
        $data = array(
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => $ParentWorkOrder,
            'Subject' => $Subject,
            'Account' => $Account,
            'Description' => $Description,
            'Address' => $Address,
            'Street' => $Street,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'PopUpReminder' => $PopUpReminder,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'PrimaryContact' => $PrimaryContact,
            'IsRecurring' => $IsRecurring,
            'SubTotal' => $SubTotal,
            'Tax' => $Tax,
            'TotalPrice' => $TotalPrice,
            'GrandTotal' => $GrandTotal,
            'Signature' => $Signature,
            'Discount' => $Discount,
            'LineItemCount' => $LineItemCount,
            'StartDate' => date("Y-m-d H:i:s",strtotime($StartDate)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDate)),
            'StartTime' => date("H:i:s",strtotime($WOStartTime)),
            'EndTime' => date("H:i:s",strtotime($WOEndTime)),
            'WORecurrenceID' => $WORecurrenceID
        );   
        $query = $this->db->insert('WorkOrder', $data);  

        API_Model::checkQuery($query);  

        $WorkOrderID = $this->db->insert_id();
 
        $query = $this->db->update('WorkOrder', array('WorkOrderNo'=>API_Controller::genCode('WO',$WorkOrderID)), array('WorkOrderID' => $WorkOrderID));   

        API_Model::checkQuery($query);  

        API_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    
        return $WorkOrderID;
    }
 
    function RecentMyWorkOrders($UserID)
    {  
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status')
        ->from('WorkOrder wo')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left') 
        ->where(array('wo.AssignedTo' => $UserID, 'wo.IsDeleted' => 0))
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }
 
    function RecentAllWorkOrders($UserID)
    {
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status')
        ->from('WorkOrder wo')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left') 
        ->where(array('wo.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query);   
        
        return $query->result();
    }

    function RecentWorkOrders($UserID)
    {  
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, a.AccountName, wot.WorkOrderType, wos.Status, wop.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName')
        ->from('WorkOrder wo')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left') 
        ->join('WorkOrderType wot','wot.WorkOrderTypeID = wo.WorkOrderType', 'left') 
        ->join('Account a','a.AccountID = wo.Account', 'left') 
        ->join('User at','at.UserID = wo.AssignedTo', 'left') 
        ->where(array('wo.IsDeleted' => 0))
        ->order_by("wo.LastModifiedDate", "desc")
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }

    function WorkOrderDetails($UserID, $WorkOrderID)
    { 
        $query = $this->db->select('wo.*, pwor.Subject as ParentWorkOrderName,ac.AccountName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName,wos.Status, woc.CategoryName, wot.WorkOrderType as WorkOrderTypeName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, wor.*, wop.Priority, DATE_FORMAT(wo.StartDate, "'.RES_DATETIME.'") as StartDate, DATE_FORMAT(wo.EndDate, "'.RES_DATETIME.'") as EndDate, DATE_FORMAT(wo.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(wo.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('WorkOrder wo')
        ->join('User cb','cb.UserID = wo.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = wo.LastModifiedBy', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->join('WORecurrence wor','wor.WORecurrenceID = wo.WORecurrenceID', 'left')
        ->join('WorkOrderType wot','wot.WorkOrderTypeID = wo.WorkOrderType', 'left')
        ->join('User at','at.UserID = wo.AssignedTo', 'left')
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = wo.WOCategory', 'left')
        ->join('Contact pc','pc.ContactID = wo.PrimaryContact', 'left')
        ->join('Account ac','ac.AccountID = wo.Account', 'left')
        ->join('WorkOrder pwor','pwor.WorkOrderID = wo.ParentWorkOrder', 'left')
        ->where(array('wo.WorkOrderID' => $WorkOrderID, 'wo.IsDeleted' => 0))
        ->get();

        API_Model::checkQuery($query); 
       
        return $query->row_array(); 
    }

    function WorkOrderRelatedList($UserID, $WorkOrderID)
    { 
        $data['WOLineItem']['title'] = 'Work Order Line Items (0)';
        $data['Chemical']['title'] = 'Chemicals (0)'; 

        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';
 
        $query = $this->db->select('Count(*) as FileCnt')
        ->get_where('File', array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        return $data;
    }

    function WorkOrderRelatedEvent($UserID, $WorkOrderID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, es.EventStatus, et.EventTypeName, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATE.'") as EventEndDate, DATE_FORMAT(e.EventStartTime, "'.RES_TIME.'") as EventStartTime, DATE_FORMAT(e.EventEndTime, "'.RES_TIME.'") as EventEndTime')
        ->from('Event e') 
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left')
        ->join('EventType et','et.EventTypeID = e.EventType', 'left')
        ->where(array('e.What' => $WorkOrderID,'e.RelatedTo' => 'WorkOrder', 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function WorkOrderRelatedFile($UserID, $WorkOrderID)
    {  
        $query = $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType')
        ->from('File f') 
        ->where(array('f.What' => $WorkOrderID,'f.RelatedTo' => 'WorkOrder', 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function WorkOrderRelatedTask($UserID, $WorkOrderID)
    {  
        $query = $this->db->select('t.TaskID, t.Subject, t.CallDisposition, tt.TaskType, tp.Priority, ts.TaskStatus')
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->where(array('t.What' => $WorkOrderID,'t.RelatedTo' => 'WorkOrder', 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EditWorkOrder($WorkOrderID, $ParentWorkOrder, $UserID, $Account, $AssignedTo, $Subject, $Description, $Address, $Street, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $PrimaryContact, $StartDate, $EndDate, $WOStartTime, $WOEndTime, $WOCategory, $SubTotal, $Tax, $TotalPrice, $GrandTotal, $Signature, $Discount, $LineItemCount, $IsRecurring, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $WORecurrenceID) {
                
       
        if(isset($IsRecurring) && $IsRecurring == 1){ 

            if($WORecurrenceID != NULL){
                $data = array(
                    'RepeatEvery' => $RepeatEvery,
                    'IntervalEvery' => $IntervalEvery,
                    'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                    'Ends' => $Ends,
                    'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                    'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                    'StartTime' => date("H:i:s",strtotime($StartTime)),
                    'EndTime' => date("H:i:s",strtotime($EndTime)),
                    'LastModifiedDate' => date("Y-m-d H:i:s")
                );   
                $query = $this->db->update('WORecurrence', $data, array('WORecurrenceID' => $WORecurrenceID));

                API_Model::checkQuery($query); 

            } else {

                 $data = array(
                    'RepeatEvery' => $RepeatEvery,
                    'IntervalEvery' => $IntervalEvery,
                    'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                    'Ends' => $Ends,
                    'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                    'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                    'StartTime' => date("H:i:s",strtotime($StartTime)),
                    'EndTime' => date("H:i:s",strtotime($EndTime)),
                    'CreatedDate' => date("Y-m-d H:i:s"),
                    'LastModifiedDate' => date("Y-m-d H:i:s")
                ); 
               
                $query = $this->db->insert('WORecurrence', $data);

                API_Model::checkQuery($query);

                $WORecurrenceID = $this->db->insert_id();
            }
        } else {
            if($WORecurrenceID != NULL){

                $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
                $WORecurrenceID = NULL;   
            }
        } 
 
        $data = array(
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => $ParentWorkOrder,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'Street' => $Street,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'PopUpReminder' => $PopUpReminder,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'PrimaryContact' => $PrimaryContact,
            'StartDate' => date("Y-m-d H:i:s",strtotime($StartDate)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDate)),
            'StartTime' => date("H:i:s",strtotime($WOStartTime)),
            'EndTime' => date("H:i:s",strtotime($WOEndTime)),
            'IsRecurring' => $IsRecurring,
            'WORecurrenceID' => $WORecurrenceID,
            'WOCategory' => $WOCategory,
            'SubTotal' => $SubTotal,
            'Tax' => $Tax,
            'TotalPrice' => $TotalPrice,
            'GrandTotal' => $GrandTotal,
            'Signature' => $Signature,
            'Discount' => $Discount,
            'LineItemCount' => $LineItemCount,
        );   
        $query = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    
        return 1;
    }

}

?>
