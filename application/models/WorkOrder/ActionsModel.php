<?php

class ActionsModel extends MY_Model{
 
    public $LoginUserID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
    }
 
    function RecentWorkOrders() 
    {
        $aColumns = array('w.WorkOrderID','w.WorkOrderNo', 'w.Subject', 'ac.AccountName', 'wot.WorkOrderType', 'wos.Status','wop.Priority', 'CONCAT(at.FirstName, " ", at.LastName)', 'DATE_FORMAT(w.StartDate, "'.RES_DATETIME.'")', 'DATE_FORMAT(w.EndDate, "'.RES_DATETIME.'")'); 
        $bColumns = array('WorkOrderID','WorkOrderNo','Subject', 'AccountName', 'WorkOrderType', 'Status', 'Priority', 'CONCAT(at.FirstName, " ", at.LastName)', 'DATE_FORMAT(w.StartDate, "'.RES_DATETIME.'")', 'DATE_FORMAT(w.EndDate, "'.RES_DATETIME.'")');
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "w.WorkOrderID";

        /* DB table to use */
        $sTable = "WorkOrder w
        LEFT JOIN WOPriority wop ON wop.WOPriorityID = w.WOPriority 
        LEFT JOIN Account ac ON ac.AccountID = w.Account 
        LEFT JOIN WorkOrderType wot ON wot.WorkOrderTypeID = w.WorkOrderType 
        LEFT JOIN User at ON at.UserID = w.AssignedTo
        LEFT JOIN WOStatus wos ON wos.WOStatusID = w.WOStatus";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY w.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
       
        $where_str = "w.IsDeleted=0";
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
  
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    //$row[] = '';
                } 
                else if($bColumns[$i] == 'Subject' || $bColumns[$i] == 'WorkOrderNo')
                {
                    $row[] = '<a href="'.SITE_URL.'WorkOrder/Actions/WorkOrderDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateWorkOrder() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => $ParentWorkOrder,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            /*'Street' => $Street,*/
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'SubTotal' => $SubTotal,
            'Tax' => $Tax,
            'TotalPrice' => $TotalPrice,
            'GrandTotal' => $GrandTotal,
            'Signature' => $Signature,
            'Discount' => $Discount,
            'LineItemCount' => $LineItemCount,
            'StartDate' => date("Y-m-d H:i:s",strtotime($StartDateTime)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDateTime)),
            'StartTime' => date("H:i:s",strtotime($StartDateTime)),
            'EndTime' => date("H:i:s",strtotime($EndDateTime)),
            'PopUpReminder' => $PopUpReminder,
            'PrimaryContact' => $PrimaryContact,
            'WORecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
            'IsRecurring' => isset($IsRecurring)?$IsRecurring:0
        );  

        $result = $this->db->insert('WorkOrder', $data); 
        $WorkOrderID = $this->db->insert_id();

        $this->db->update('WorkOrder', array('WorkOrderNo'=>'WO'.'-'.sprintf("%05d", $WorkOrderID)), array('WorkOrderID' => $WorkOrderID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

        $data = array(
            'Subject' => $Subject,
            'RelatedTo' => 'WorkOrder',
            'What' => $WorkOrderID,
            'AssignedTo' => $AssignedTo,
            'Who' => $PrimaryContact,
            'Description' => $Description,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($StartDateTime)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDateTime)),
            'EventStartTime' => date("H:i:s", strtotime($StartDateTime)),
            'EventEndTime' => date("H:i:s", strtotime($EndDateTime)),
            'RecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0
        );
        $result = $this->db->insert('Event', $data);  

        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));
    
        return $result;
    } 

    function saveRecurrenceSetting()
    { 
        extract($_POST);

        if(isset($WORecurrenceID) && !empty($WORecurrenceID))
        {
            $data = array(
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->update('WORecurrence', $data, array('WORecurrenceID'=>$WORecurrenceID));    

        } else {
            $data = array(
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->insert('WORecurrence', $data);    
            $WORecurrenceID = $this->db->insert_id();
        }
        return $WORecurrenceID;
    }
 
    function EditWorkOrder($WorkOrderID) {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => $ParentWorkOrder,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
           /* 'Street' => $Street,*/
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'SubTotal' => $SubTotal,
            'Tax' => $Tax,
            'TotalPrice' => $TotalPrice,
            'GrandTotal' => $GrandTotal,
            'Signature' => $Signature,
            'Discount' => $Discount,
            'LineItemCount' => $LineItemCount,
            /*'StartDate' => date("Y-m-d H:i:s",strtotime($StartDateTime)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDateTime)),
            'StartTime' => date("H:i:s",strtotime($StartDateTime)),
            'EndTime' => date("H:i:s",strtotime($EndDateTime)),*/
            'PrimaryContact' => $PrimaryContact,
            'PopUpReminder' => $PopUpReminder,
            'WORecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
            'IsRecurring' => isset($IsRecurring)?$IsRecurring:0
        ); 
  
        $result = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID));    
    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    
        return $result;
    } 

    function removeRecurrenceSetting()
    {
        $WORecurrenceID = $_REQUEST['WORecurrenceID'];
        $WorkOrderID = $_REQUEST['WorkOrderID'];
        $this->db->update('WorkOrder',  array('IsRecurring' => 0), array('WorkOrderID' => $WorkOrderID));  
        return $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
    }

    function getWorkOrderData($WorkOrderID, $RelatedObjName = 'AllObject')
    {
        $result = $this->db->update('WorkOrder', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('WorkOrderID' => $WorkOrderID));  

        $query = $this->db->select('w.*, ac.AccountName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pm.FirstName, " ", pm.LastName) as PrimaryContactName, wos.Status, wop.Priority, woc.CategoryName, pwo.Subject as ParentWorkOrderName, wot.WorkOrderType as WorkOrderTypeName, DATE_FORMAT(w.StartDate, "'.RES_DATETIME.'") as StartDate, DATE_FORMAT(w.EndDate, "'.RES_DATETIME.'") as EndDate, DATE_FORMAT(w.EndTime, "'.RES_TIME.'") as EndTime,DATE_FORMAT(w.StartTime, "'.RES_TIME.'") as StartTime, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(w.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(w.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate')
        ->from('WorkOrder w')
        ->join('Account ac','ac.AccountID = w.Account', 'left')
        ->join('User at','at.UserID = w.AssignedTo', 'left')
        ->join('Contact pm','pm.ContactID = w.PrimaryContact', 'left')
        ->join('WorkOrder pwo','pwo.WorkOrderID = w.ParentWorkOrder', 'left')
        ->join('WorkOrderType wot','wot.WorkOrderTypeID = w.WorkOrderType', 'left')
        ->join('WOStatus wos','wos.WOStatusID = w.WOStatus', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = w.WOPriority', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = w.WOCategory', 'left')
        ->join('User cb','cb.UserID = w.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = w.LastModifiedBy', 'left')
        ->where(array('w.WorkOrderID' => $WorkOrderID)) 
        ->get();
        $data['WorkOrderData'] = $query->row();

        if(isset($data['WorkOrderData']->WORecurrenceID)){
            $query = $this->db->select('wor.RepeatEvery, wor.IntervalEvery, wor.RepeatOn, wor.Ends, wor.EndsOnDate, wor.EndsAfterOccurrences, wor.StartTime, wor.EndTime')
            ->from('WORecurrence wor')
            ->where(array('wor.WORecurrenceID' => $data['WorkOrderData']->WORecurrenceID)) 
            ->get();
            $data['WORecurrence'] = $query->row();
        } 

        if($RelatedObjName == 'WOChemicalLineItems'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('wocli.*, c.ChemicalName');
            $this->db->from('WOChemicalLineItem wocli');
            $this->db->join('Chemical c','c.ChemicalID = wocli.Chemical', 'left');
            $this->db->where(array('wocli.WorkOrder' => $WorkOrderID, 'wocli.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['WOChemicalLineItems'] = $query->result();
        }

        if($RelatedObjName == 'WOLineItems'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('woli.*, p.ProductName');
            $this->db->from('WOLineItem woli');
            $this->db->join('Product p','p.ProductID = woli.Product', 'left');
            $this->db->where(array('woli.WorkOrder' => $WorkOrderID, 'woli.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['WOLineItems'] = $query->result();
        }
    
        if($RelatedObjName == 'Events'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('e.EventID, e.AssignedTo, e.Subject, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName,  CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate');
            $this->db->from('Event e');
            $this->db->join('User cb','cb.UserID = e.CreatedBy', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->join('Contact pc','pc.ContactID = e.Who', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->where(array('e.What' => $WorkOrderID,'e.RelatedTo' => 'WorkOrder', 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Events'] = $query->result();
        }

        if($RelatedObjName == 'Files'  || $RelatedObjName == 'AllObject'){
            $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy');
            $this->db->from('File f');
            $this->db->join('User cb','cb.UserID = f.CreatedBy', 'left');
            $this->db->where(array('f.What' => $WorkOrderID,'f.RelatedTo' => 'WorkOrder', 'f.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Files'] = $query->result();
        }

        if($RelatedObjName == 'Tasks'  || $RelatedObjName == 'AllObject'){
           $this->db->select('t.TaskID, t.Subject, tt.TaskType, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate, tp.Priority, ts.TaskStatus, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName');
            $this->db->from('Task t'); 
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('Contact pc','pc.ContactID = t.Who', 'left');
            $this->db->where(array('t.What' => $WorkOrderID,'t.RelatedTo' => 'WorkOrder', 't.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Tasks'] = $query->result();
        }

        if($RelatedObjName == 'Notes'  || $RelatedObjName == 'AllObject'){
           $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName');
            $this->db->from('Note n'); 
            $this->db->join('User o','o.UserID = n.Owner', 'left');
            $this->db->where(array('n.What' => $WorkOrderID,'n.RelatedTo' => 'WorkOrder', 'n.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Notes'] = $query->result();
        }
        return $data;
    }

    function CreateNote($WorkOrderID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'Owner' => $UserID,
            'What' => $WorkOrderID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'WorkOrder'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;

    }

    function SaveScheduling(){

        extract($_POST); 

        $UserID = $this->LoginUserID;
  
         $data = array(
            'Subject' => $Subject,
            'RelatedTo' => 'WorkOrder',
            'What' => $WorkOrderID,
            'AssignedTo' => $AssignedTo,
            'Who' => $Who,
            'Description' => $Description,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s", strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s", strtotime($EventEndDate)),
        );
        $result = $this->db->insert('Event', $data);  

        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        if($result){

            $this->UpdateWOStartEndDateTime($WorkOrderID);

            $UserRes = $query = $this->db->get_where('User', array('UserID' => $AssignedTo));  
            $User = $UserRes->row();

            return $this->GetSchedulingHtml($EventID, $User->FirstName, $User->LastName, $EventStartDate, $EventEndDate);
        }
    }

    function GetSchedulingHtml($EventID, $FirstName, $LastName, $EventStartDate, $EventEndDate)
    {
        $SchedulingWrap = "";
        $SchedulingWrap .= '<div id="Event'.$EventID.'"><div class="form-wrap">
                        <form action="#" class="form-horizontal">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-3 text-left txt-dark">
                                    Assigned To
                                </div>
                                <div class="col-md-4" id="AssignedToDropDown'.$EventID.'">
                                    '.$FirstName.' '.$LastName.'
                                </div>
                                <div class="col-md-4" id="AssignedToBtn'.$EventID.'">
                                    <a href="#">Change</a>
                                </div>
                                <div class="col-md-1 pl-10">
                                    <div class="dropdown" >
                                        <a data-toggle="dropdown"><i class="fa fa-toggle-down"></i></a>
                                        <ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu" style="right:0;left: unset;">
                                            <li>
                                                <a>View in Calendar</a>
                                                <a OnClick="EditScheduling('.$EventID.')">Edit</a>
                                                <a OnClick="DeleteScheduling('.$EventID.')">Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pt-5">
                                <div class="col-md-3 text-left txt-dark">
                                    Start
                                </div>
                                <div class="col-md-6">
                                    '.$EventStartDate.'
                                </div>
                            </div>
                            <div class="form-group pt-5">
                                <div class="col-md-3 text-left txt-dark">
                                    End
                                </div>
                                <div class="col-md-6">
                                    '.$EventEndDate.'
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="light-grey-hr">
                    </div>';
        return $SchedulingWrap;
    }

    function UpdateScheduling($EventID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
         $data = array(
            'AssignedTo' => $AssignedTo,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s", strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s", strtotime($EventEndDate)),
        );
        $result = $this->db->update('Event', $data, array('EventID' => $EventID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        if($result){
 
            $this->UpdateWOStartEndDateTime($WorkOrderID);
            $UserRes = $query = $this->db->get_where('User', array('UserID' => $AssignedTo));  
            $User = $UserRes->row();

            return $this->GetSchedulingHtml($EventID, $User->FirstName, $User->LastName, $EventStartDate, $EventEndDate);
        }
    }

    function UpdateWOStartEndDateTime($WorkOrderID)
    {
        $UserID = $this->LoginUserID;
 
        $query = $this->db->select('MIN(EventStartDate) as MinEventStartDate, MAX(EventEndDate) as MaxEventEndDate, MIN(EventStartDate) as MinEventStartTime, MAX(EventEndDate) as MaxEventEndTime')
        ->from('Event')
        ->where(array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID))
        ->get(); 
        $Event = $query->row(); 
  
        $data = array(
            'StartDate' => date("Y-m-d H:i:s", strtotime($Event->MinEventStartDate)),
            'EndDate' => date("Y-m-d H:i:s", strtotime($Event->MaxEventEndDate)),
            'StartTime' => date("H:i:s", strtotime($Event->MinEventStartTime)),
            'EndTime' => date("H:i:s", strtotime($Event->MaxEventEndTime)),
        ); 
        $result = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    }

    function DeleteScheduling($EventID)
    {
        return $this->db->delete('Event', array('EventID' => $EventID)); 
    }

    function GetScheduling($EventID)
    {
        $query = $this->db->get_where('Event', array('EventID' => $EventID)); 
        $Event = $query->row(); 

        $SchedulingWrap = "";
        $SchedulingWrap .= '<div id="Event'.$EventID.'"><div class="form-wrap">
                   <form action="#" class="form-horizontal" id="SchedulingForm'.$EventID.'">
                    <input type="hidden" name="WorkOrderID" value="'.$Event->What.'">
                   <div class="form-body">
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">Assigned To</label>
                           <div class="col-md-6">
                               <select name="AssignedTo" id="AssignedTo" class="form-control select2">
                               <option value="">Select Owner</option>';
                                 $Users = $this->ActionsModel->GetUsers();
                                    foreach ($Users as $User) { 
                                        if($Event->AssignedTo == $User->UserID){
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'" selected="">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        } else {
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        }
                                    } 
                            $SchedulingWrap .= '</select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">Start</label>
                           <div class="col-md-6">
                               <input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="'.date("m/d/Y h:i A", strtotime($Event->EventStartDate)).'" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">End</label>
                           <div class="col-md-6">
                               <input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="'.date("m/d/Y h:i A", strtotime($Event->EventEndDate)).'" required>
                           </div>
                       </div>
                   </div>
                   <div class="pull-right">
                       <button type="button" class="btn btn-default" OnClick="UpdateScheduling('.$EventID.')" data-dismiss="modal">Close</button>
                       <button type="button" class="btn btn-primary" OnClick="UpdateScheduling('.$EventID.')">Save</button>
                   </div>
                   </form>
                 <div class="clearfix"></div>
                 <hr class="light-grey-hr">
                 </div>
                 </div>';

        return $SchedulingWrap;
    }

    function EditSchAssignedTo()
    { 
        extract($_POST); 
        $SchedulingWrap = "";
        $SchedulingWrap .= '<select name="AssignedTo" id="AssignedToVal'.$EventID.'" class="form-control select2">
                               <option value="">Select Owner</option>';
                                 $Users = $this->ActionsModel->GetUsers();
                                    foreach ($Users as $User) { 
                                        if($AssignedTo == $User->UserID){
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'" selected="">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        } else {
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        }
                                    } 
                            $SchedulingWrap .= '</select>';
        return $SchedulingWrap;

    }

    function UpdateAssignedTo()
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $result = $this->db->update('Event', array('AssignedTo' => $AssignedTo), array('EventID' => $EventID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        $UserRes = $query = $this->db->get_where('User', array('UserID' => $AssignedTo));  
        $User = $UserRes->row(); 

        return $User->FirstName.' '.$User->LastName;
    }

    function WOLineItem($WorkOrderID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = 0;
        if(count($Product) > 0){ 

            $res = $this->db->delete('WOLineItem', array('WorkOrder' => $WorkOrderID));

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key]) && !empty($ListPrice[$key]) && !empty($UnitPrice[$key]) && !empty($Quantity[$key]) && !empty($SubTotal[$key]) && !empty($NetTotal[$key])){
                    $data = array(
                        'WorkOrder' => $WorkOrderID,
                        'Product' => $Product[$key],
                        'ListPrice' => $ListPrice[$key],
                        'Discount' => $Discount[$key],
                        'UnitPrice' => $UnitPrice[$key],
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => $SubTotal[$key],
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'NetTotal' => $NetTotal[$key]
                    );     
                    $query = $this->db->insert('WOLineItem', $data);  
                    $WOLineItemID = $this->db->insert_id();

                    $this->db->update('WOLineItem', array('LineItemNo'=>'WOLI'.'-'.sprintf("%05d", $WorkOrderID)), array('WOLineItemID' => $WOLineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'WOLineItem', array('WOLineItemID' => $WOLineItemID));
                }
            }
        }

        return $query;
    }

    function GetPrimaryContactOptions()
    { 

        $query = $this->db->get_where('Contact', array('IsActive' => 1, 'IsDeleted' => 0, 'Account' => $_POST['AccountID'])); 
        $results = $query->result();
        $PCOptions = '<option value="">Select Contact</option>';
        foreach ($results as $key => $value) {
            if(isset($_POST['PrimaryContact']) && !empty($_POST['PrimaryContact']) && $_POST['PrimaryContact'] == $value->ContactID)
                $PCOptions .= '<option value="'.$value->ContactID.'" selected="">'.$value->FirstName.' '.$value->LastName.'</option>';
            else 
                 $PCOptions .= '<option value="'.$value->ContactID.'">'.$value->FirstName.' '.$value->LastName.'</option>';
        }
        return $PCOptions;
    }

    function GetProductDetails($ProductID)
    {
        $query = $this->db->get_where('Product', array('ProductID' => $ProductID)); 
        return json_encode($query->row()); 
    }
  
    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetWorkOrderTypes()
    { 
        $query = $this->db->get('WorkOrderType'); 
        
        return $query->result();
    }

    function GetWOStatus()
    { 
        $query = $this->db->get('WOStatus'); 
        
        return $query->result();
    }

    function WOPriorities()
    { 
        $query = $this->db->get('WOPriority'); 
        
        return $query->result();
    }

    function WOCategories()
    { 
        $query = $this->db->get('WOCategory'); 
        
        return $query->result();
    }

    function GetAccounts()
    { 
        $UserID = $this->session->userdata('UserID');

        $query = $this->db->get_where('Account',array('AssignedTo' => $UserID)); 
        
        return $query->result();
    }

    function GetContacts()
    { 
        $query = $this->db->get('Contact'); 
        
        return $query->result();
    }

    function GetWorkOrders()
    { 
        $query = $this->db->get('WorkOrder'); 
        
        return $query->result();
    }

    function GetProducts()
    {
        $query = $this->db->get('Product'); 
        
        return $query->result();
    }

    function GetChemicals()
    {
        $query = $this->db->get('Chemical'); 
        
        return $query->result();
    }
 
    function GetViews(){

        $query = $this->db->query("SELECT * FROM WorkOrderView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') ORDER BY WorkOrderViewName ASC");
        
        return $query->result();
    }

    function DeleteWorkOrder($WorkOrderID)
    {
        return $this->db->update('WorkOrder', array('IsDeleted' => 1), array('WorkOrderID' => $WorkOrderID));  
    }
}

?>
